﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBridge.Api.DAL
{
    public class DataTable
    {
    
        public int Id { get; set; }
        public string Name { get; set; }
        public DataType Type { get; set; }
        public string Value { get; set; }
        public byte[] Image { get; set; }
        public int? ExcelFileId { get; set; }
        public virtual Excel ExcelFile { get; set; }
    }

    public enum DataType
    {
        InfoPoint = 0,
        InfoTable = 1
    }
}
