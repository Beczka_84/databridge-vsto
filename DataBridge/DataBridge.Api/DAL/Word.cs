﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBridge.Api.DAL
{
    public class Word
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string FullPath { get; set; }

        public virtual ICollection<Excel> ConnectedExcels { get; set; }

        public Word()
        {
            this.ConnectedExcels = new List<Excel>();
        }
    }
}
