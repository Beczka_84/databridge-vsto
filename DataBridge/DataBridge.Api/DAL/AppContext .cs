﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBridge.Api.DAL
{
   
    public class AppContext : DbContext
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public AppContext() : base("AppContext")
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            Database.SetInitializer<AppContext>(new CreateDatabaseIfNotExists<AppContext>());
            //Database.Log = x => log.Info(x);
        }

        public virtual DbSet<Excel> Excels { get; set; }
        public virtual DbSet<Word> Words { get; set; }
        public virtual DbSet<DataTable> DataTables { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Word>()
        //        .HasOptional(p => p.ConnectedExcels)
        //        .WithMany()
        //        .WillCascadeOnDelete(true);
        //}

    }
}
