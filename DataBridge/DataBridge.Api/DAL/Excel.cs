﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBridge.Api.DAL
{
    public class Excel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string FullPath { get; set; }
        public int? ConnectedWordId { get; set; }
        public virtual Word ConnectedWord { get; set; }
        public virtual ICollection<DataTable> DataTables { get; set; }

        public Excel()
        {
            this.DataTables = new List<DataTable>();
        }

    }
}
