﻿using DataBridge.Api.DAL;
using DataBridge.Api.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBridge.Api.Service
{
    public class ExcelService
    {
        private readonly AppContext _db;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ExcelService()
        {
            _db = new AppContext();
        }

        public ServiceResult AssociateWord(WordVM wordModel, ExcelVM excelModel)
        {
            try
            {
                Excel excel;

                if (_db.Excels.Any(x => x.FullPath == excelModel.FullPath))
                {
                    excel = _db.Excels.FirstOrDefault(x => x.FullPath == excelModel.FullPath);
                }
                else
                {
                    excel = new Excel();
                    excel.Name = excelModel.Name;
                    excel.Path = excelModel.Path;
                    excel.FullPath = excelModel.FullPath;
                    _db.Excels.Add(excel);
                }


                if (_db.Words.Any(x => x.FullPath == wordModel.FullPath))
                {
                    Word word = _db.Words.FirstOrDefault(x => x.FullPath == wordModel.FullPath);
                    if (word.ConnectedExcels.Any(x => x.FullPath == excelModel.FullPath))
                    {
                        return new ServiceResult() { Sucess = false, ErrorMessage = "An Excel file with the same name has already been associated with the word document please change the name and associate again." };
                    }
                    word.ConnectedExcels.Add(excel);
                    _db.Words.Attach(word);
                    _db.Entry(word).State = EntityState.Modified;
                }
                else
                {
                    Word word = new Word();
                    word.Name = wordModel.Name;
                    word.Path = wordModel.Path;
                    word.FullPath = wordModel.FullPath;
                    word.ConnectedExcels.Add(excel);
                    _db.Words.Add(word);
                }

                _db.SaveChanges();
                return new ServiceResult() { Sucess = true, ErrorMessage = "" };
            }
            catch (DbUpdateException e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }

            catch (Exception e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }

        }

        public ServiceResult DissociateWord(WordVM wordModel, ExcelVM excelModel)
        {
            try
            {
                if (_db.Excels.Any(x => x.FullPath == excelModel.FullPath))
                {
                    Excel excel = _db.Excels.FirstOrDefault(x => x.FullPath == excelModel.FullPath);
                    if (_db.Words.Any(x => x.FullPath == wordModel.FullPath))
                    {
                        Word word = _db.Words.FirstOrDefault(x => x.FullPath == wordModel.FullPath);
                        word.ConnectedExcels.Remove(excel);
                        //_db.Words.Add(word);
                    }
                    else
                    {
                        return new ServiceResult() { Sucess = false, ErrorMessage = "No Word File Associated to this Excel" };
                    }
                }
                _db.SaveChanges();
                return new ServiceResult() { Sucess = true, ErrorMessage = "" };
            }
            catch (DbUpdateException e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }

            catch (Exception e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }
        }

        public Word GetAssociatedWord(ExcelVM model)
        {
            return _db.Excels.FirstOrDefault(x => x.Name == model.Name && x.Path == model.Path)?.ConnectedWord ?? null;
        }

        public IEnumerable<DataTable> GetDataTables(ExcelVM model)
        {
            return _db.Excels.FirstOrDefault(x => x.FullPath == model.FullPath)?.DataTables.ToList() ?? Enumerable.Empty<DataTable>();
        }

        public ServiceResult CreateDataTable(ExcelVM excelModel, DataTableVM dataTableModel)
        {
            Excel excel;
            bool newItem = false;

            try
            {
                if (_db.Excels.Any(x => x.FullPath == excelModel.FullPath))
                {
                    excel = _db.Excels.FirstOrDefault(x => x.FullPath == excelModel.FullPath);
                }
                else
                {
                    excel = new Excel();
                    excel.Name = excelModel.Name;
                    excel.Path = excelModel.Path;
                    excel.FullPath = excelModel.FullPath;
                    newItem = true;
                }

                if (excel.DataTables.Any(x => x.Name == dataTableModel.Name))
                {
                    DataTable dataTableToDelete = excel.DataTables.FirstOrDefault(x => x.Name == dataTableModel.Name);
                    excel.DataTables.Remove(dataTableToDelete);
                }
                DataTable dataTable = new DataTable();
                dataTable.Image = dataTableModel.Image;
                dataTable.Name = dataTableModel.Name;
                dataTable.Type = dataTableModel.Type;
                dataTable.Value = dataTableModel.Value;
                excel.DataTables.Add(dataTable);

                if (newItem)
                {
                    _db.Excels.Add(excel);
                }
                else
                {
                    _db.Excels.Attach(excel);
                    _db.Entry(excel).State = EntityState.Modified;
                }

                _db.SaveChanges();
                return new ServiceResult() { Sucess = true, ErrorMessage = "" };
            }
            catch (DbUpdateException e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }

            catch (Exception e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }
        }

        public ServiceResult UpdateDataTables(ExcelVM excelModel, List<DataTableVM> listOfDataTable)
        {
            Excel excel;
            bool newItem = false;

            try
            {
                if (_db.Excels.Any(x => x.FullPath == excelModel.FullPath))
                {
                    excel = _db.Excels.FirstOrDefault(x => x.FullPath == excelModel.FullPath);
                }
                else
                {
                    excel = new Excel();
                    excel.Name = excelModel.Name;
                    excel.Path = excelModel.Path;
                    excel.FullPath = excelModel.FullPath;
                    _db.Excels.Add(excel);
                    newItem = true;
                }
                excel.DataTables.ToList().ForEach(x => _db.DataTables.Remove(x));
                excel.DataTables.Clear();

                foreach (DataTableVM item in listOfDataTable)
                {
                    DataTable dataTable = new DataTable();
                    dataTable.Image = item.Image;
                    dataTable.Name = item.Name;
                    dataTable.Type = item.Type;
                    dataTable.Value = item.Value;
                    excel.DataTables.Add(dataTable);
                }


                if (newItem)
                {
                    _db.Excels.Add(excel);
                }
                else
                {
                    _db.Excels.Attach(excel);
                    _db.Entry(excel).State = EntityState.Modified;
                }

                _db.SaveChanges();
                return new ServiceResult() { Sucess = true, ErrorMessage = "" };
            }
            catch (DbUpdateException e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }

            catch (Exception e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }
        }

        public ServiceResult UpdateDataTable(ExcelVM excelModel, DataTableVM DataTable)
        {
            Excel excel;
            bool newItem = false;

            try
            {
                if (_db.Excels.Any(x => x.FullPath == excelModel.FullPath))
                {
                    excel = _db.Excels.FirstOrDefault(x => x.FullPath == excelModel.FullPath);
                }
                else
                {
                    excel = new Excel();
                    excel.Name = excelModel.Name;
                    excel.Path = excelModel.Path;
                    excel.FullPath = excelModel.FullPath;
                    _db.Excels.Add(excel);
                }

                DataTable original = excel.DataTables.FirstOrDefault(x => x.Name == DataTable.Name);

                if (original != null)
                {
                    original.Image = DataTable.Image;
                    original.Name = DataTable.Name;
                    original.Type = DataTable.Type;
                    original.Value = DataTable.Value;
                    _db.DataTables.Attach(original);
                    _db.Entry(original).State = EntityState.Modified;
                }
                _db.SaveChanges();
                return new ServiceResult() { Sucess = true, ErrorMessage = "" };
            }
            catch (DbUpdateException e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }

            catch (Exception e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }
        }

        public ServiceResult ClearDB()
        {
            try
            {
                List<int> ids = new List<int>();
                foreach (var excel in _db.Excels)
                {

                    if (!File.Exists(excel.FullPath))
                    {
                        ids.Add(excel.Id);
                        _db.Excels.Remove(excel);
                    }
                }

                _db.DataTables.RemoveRange(_db.DataTables.Where(x => ids.Contains((int)x.ExcelFileId)));

                foreach (var word in _db.Words)
                {

                    if (!File.Exists(word.FullPath))
                    {
                        _db.Words.Remove(word);
                    }
                }

                _db.SaveChanges();
                return new ServiceResult() { Sucess = true, ErrorMessage = "" };
            }
            catch (DbUpdateException e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }

            catch (Exception e)
            {
                log.Error(e.Message);
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }
        }
    }
}
