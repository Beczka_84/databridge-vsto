﻿using DataBridge.Api.DAL;
using DataBridge.Api.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBridge.Api.Service
{
    public class WordService
    {
        private readonly AppContext _db;

        public WordService()
        {
            _db = new AppContext();
        }

        public int GetAssociatedExcelsCount(WordVM model)
        {
            return _db.Words.FirstOrDefault(x => x.Name == model.Name && x.Path == model.Path)?.ConnectedExcels.Count() ?? 0;
        }

        public IEnumerable<Excel> GetAssociatedExcels(WordVM model)
        {
            return _db.Words.FirstOrDefault(x => x.Name == model.Name && x.Path == model.Path)?.ConnectedExcels ?? Enumerable.Empty<Excel>();
        }

        public ServiceResult AssociateExcel(WordVM wordModel, ExcelVM excelModel)
        {
            try
            {
                Excel excel;
                Word word;



                if (_db.Excels.Any(x => x.FullPath == excelModel.FullPath))
                {
                    excel = _db.Excels.FirstOrDefault(x => x.FullPath == excelModel.FullPath);
                    if (excel.ConnectedWord != null)
                    {
                        return new ServiceResult() { Sucess = false, ErrorMessage = "An Excel file with the same name has already been associated with the word document please change the name and associate again." };
                    }

                }
                else
                {
                    excel = new Excel();
                    excel.Name = excelModel.Name;
                    excel.Path = excelModel.Path;
                    excel.FullPath = excelModel.FullPath;
                    _db.Excels.Add(excel);
                }


                if (_db.Words.Any(x => x.FullPath == wordModel.FullPath))
                {
                    word = _db.Words.FirstOrDefault(x => x.FullPath == wordModel.FullPath);
                    word.ConnectedExcels.Add(excel);
                    _db.Words.Attach(word);
                    _db.Entry(word).State = EntityState.Modified;
                }
                else
                {
                    word = new Word();
                    word.Name = wordModel.Name;
                    word.Path = wordModel.Path;
                    word.FullPath = wordModel.FullPath;
                    word.ConnectedExcels.Add(excel);
                    _db.Words.Add(word);
                }             
                _db.SaveChanges();
                return new ServiceResult() { Sucess = true, ErrorMessage = "" };
            }
            catch (DbUpdateException e)
            {
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }

            catch (Exception e)
            {
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }

        }

        public ServiceResult ClearWord(WordVM wordModel)
        {
            try
            {
                if (_db.Words.Any(x => x.FullPath == wordModel.FullPath))
                {
                    Word word = _db.Words.FirstOrDefault(x => x.FullPath == wordModel.FullPath);
                    if (word != null)
                    {
                        word.ConnectedExcels.Clear();
                    }
                }
                _db.SaveChanges();
                return new ServiceResult() { Sucess = true, ErrorMessage = "" };
            }
            catch (DbUpdateException e)
            {
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }

            catch (Exception e)
            {
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }
        }

        public ServiceResult DissociateExcel(WordVM wordModel, ExcelVM excelModel)
        {
            try
            {
                if (_db.Words.Any(x => x.FullPath == wordModel.FullPath))
                {
                    Word word = _db.Words.FirstOrDefault(x => x.FullPath == wordModel.FullPath);
                    Excel excel = word.ConnectedExcels.FirstOrDefault(x => x.FullPath == excelModel.FullPath);
                    if (excel != null)
                    {
                        word.ConnectedExcels.Remove(excel);
                    }
                    else
                    {
                        return new ServiceResult() { Sucess = false, ErrorMessage = "Selected excel file is not associated with this document" };
                    }
                }
                _db.SaveChanges();
                return new ServiceResult() { Sucess = true, ErrorMessage = "" };
            }
            catch (DbUpdateException e)
            {
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }

            catch (Exception e)
            {
                return new ServiceResult() { Sucess = false, ErrorMessage = e.Message };
            }
        }
        public IEnumerable<DataTable> GetDataTables(WordVM wordModel, ExcelVM excelModel)
        {
            if (_db.Words.Any(x => x.FullPath == wordModel.FullPath))
            {
                Word word = _db.Words.FirstOrDefault(x => x.FullPath == wordModel.FullPath);
                Excel excel = word.ConnectedExcels.FirstOrDefault(x => x.FullPath == excelModel.FullPath);
                if (excel != null)
                {
                   return excel.DataTables;
                }
                else
                {
                    return Enumerable.Empty<DataTable>();
                }
            }
            return Enumerable.Empty<DataTable>();
        }
    }
}
