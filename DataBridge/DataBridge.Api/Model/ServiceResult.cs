﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBridge.Api.Model
{
    public class ServiceResult
    {
        public bool Sucess { get; set; }
        public string ErrorMessage { get; set; }
    }
}
