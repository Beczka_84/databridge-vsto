﻿using DataBridge.Api.DAL;

namespace DataBridge.Api.Model
{
    public class DataTableVM
    {
        public string Name { get; set; }
        public DataType Type { get; set; }
        public string Value { get; set; }
        public byte[] Image { get; set; }
    }
}
