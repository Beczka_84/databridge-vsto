﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBridge.Api.Model
{
    public class ExcelVM
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string FullPath {
            get
            {
                return string.Join(@"\", this.Path, this.Name);
            }
        }
    }
}
