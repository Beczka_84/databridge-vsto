﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataBridge.Api.DAL;
using DataBridge.Api.Model;
using DataBridge.Api.Service;
using DataBridge.Excel.Services;
using WordApp = Microsoft.Office.Interop.Word;
using Microsoft.Office.Core;

namespace DataBridge.Excel
{
    public partial class ProgressScanCC : Form
    {
        private ExcelVM _excelVM { get; set; }
        private WordService _wordService { get; set; }
        private WordVM _wordVM { get; set; }
        private List<string> _namesList { get; set; }

        public ProgressScanCC(ExcelVM excelVM, WordService wordService, WordVM wordVM, List<string> namesList)
        {
            InitializeComponent();

            this._namesList = namesList;
            this._excelVM = excelVM;
            this._wordService = wordService;
            this._wordVM = wordVM;

            this.Show();
        }

        private void ProgressScanCC_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Globals.ThisAddIn.Application.Visible = false;
            string[] progIds = { "Word.Application" };
            WordApp.Application app = null;
            WordApp.Document doc = null;
            try
            {
                WordInstances wordInstances = new WordInstances();
                List<object> instances = wordInstances.GetRunningInstances(progIds);


                foreach (WordApp.Application wordApp in instances)
                {
                    app = wordApp;
                    foreach (WordApp.Document singleDoc in app.Documents)
                    {
                        if (singleDoc.Name == Path.GetFileName(_wordVM.FullPath))
                        {
                            this.label1.Text = "Attaching to open word document ....";
                            doc = singleDoc;
                            break;
                        }
                    }
                }
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                if (File.Exists(_wordVM.FullPath))
                {
                    this.label1.Text = "Opening word document ....";
                    app = new WordApp.Application();
                    doc = app.Documents.Open(_wordVM.FullPath);
                }
            }

            if (doc == null)
            {
                if (File.Exists(_wordVM.FullPath))
                {
                    this.label1.Text = "Opening word document ....";
                    app = new WordApp.Application();
                    doc = app.Documents.Open(_wordVM.FullPath);
                }
                else
                {
                    MessageBox.Show(string.Format("File : {0} don't exist", _wordVM.FullPath), "DataBridge.Excel");
                    return;
                }
            }

            try
            {
                app.Visible = false;
                app.ScreenUpdating = false;
                IEnumerable<Api.DAL.DataTable> dataTables = _wordService.GetDataTables(_wordVM, _excelVM);
                Api.DAL.Excel excel = _wordService.GetAssociatedExcels(_wordVM).FirstOrDefault(x => x.FullPath == _excelVM.FullPath);

                if (excel == null)
                {
                    Globals.ThisAddIn.Application.Visible = true;
                    this.Close();
                }

                if (excel.DataTables == null)
                {
                    Globals.ThisAddIn.Application.Visible = true;
                    this.Close();
                }

                this.label1.Text = "Starting ....";
                this.progressBar1.Maximum = doc.ContentControls.Count;

                for (int i = 0; i < doc.ContentControls.Count; i++)
                {
                    this.label1.Text = string.Format("scaning word content controls {0} of {1}", i + 1, doc.ContentControls.Count);
                    this.progressBar1.Value = i + 1;

                    Microsoft.Office.Interop.Word.ContentControl item = doc.ContentControls[i + 1];

                    if (_namesList.Contains(item.Title))
                    {
                        if (item.Title.Contains(Constants.IPPrefix))
                        {
                            item.Tag = excel.Id.ToString();
                        }

                        if (item.Title.Contains(Constants.TPPrefix))
                        {
                            item.Tag = excel.Id.ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error : {0}", ex.Message), "DataBridge.Excel");
                doc.Close();
                app.Quit();
                Globals.ThisAddIn.Application.Visible = true;
                this.Close();
            }
            app.ScreenUpdating = true;
            app.Visible = true;
            Globals.ThisAddIn.Application.Visible = true;
            this.Close();
        }
    }
}
