﻿namespace DataBridge.Excel
{
    partial class ExcelRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ExcelRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DATA_BRIDGE = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.AssociateButton = this.Factory.CreateRibbonButton();
            this.DissociateButton = this.Factory.CreateRibbonButton();
            this.RefreshBtn = this.Factory.CreateRibbonButton();
            this.UpdateAllButton = this.Factory.CreateRibbonButton();
            this.UpdateWorksheet = this.Factory.CreateRibbonButton();
            this.separator1 = this.Factory.CreateRibbonSeparator();
            this.CreateInfoPointButton = this.Factory.CreateRibbonButton();
            this.ListInfoPoints = this.Factory.CreateRibbonDropDown();
            this.separator3 = this.Factory.CreateRibbonSeparator();
            this.CreateTablePicButton = this.Factory.CreateRibbonButton();
            this.ListTablePics = this.Factory.CreateRibbonDropDown();
            this.separator2 = this.Factory.CreateRibbonSeparator();
            this.Associatedfile = this.Factory.CreateRibbonEditBox();
            this.DATA_BRIDGE.SuspendLayout();
            this.group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DATA_BRIDGE
            // 
            this.DATA_BRIDGE.Groups.Add(this.group1);
            this.DATA_BRIDGE.Label = "DATA BRIDGE";
            this.DATA_BRIDGE.Name = "DATA_BRIDGE";
            // 
            // group1
            // 
            this.group1.Items.Add(this.AssociateButton);
            this.group1.Items.Add(this.DissociateButton);
            this.group1.Items.Add(this.RefreshBtn);
            this.group1.Items.Add(this.UpdateAllButton);
            this.group1.Items.Add(this.UpdateWorksheet);
            this.group1.Items.Add(this.separator1);
            this.group1.Items.Add(this.CreateInfoPointButton);
            this.group1.Items.Add(this.ListInfoPoints);
            this.group1.Items.Add(this.separator3);
            this.group1.Items.Add(this.CreateTablePicButton);
            this.group1.Items.Add(this.ListTablePics);
            this.group1.Items.Add(this.separator2);
            this.group1.Items.Add(this.Associatedfile);
            this.group1.Name = "group1";
            // 
            // AssociateButton
            // 
            this.AssociateButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.AssociateButton.Label = "Associate";
            this.AssociateButton.Name = "AssociateButton";
            this.AssociateButton.OfficeImageId = "ExportWord";
            this.AssociateButton.ShowImage = true;
            this.AssociateButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AssociateButton_Click);
            // 
            // DissociateButton
            // 
            this.DissociateButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.DissociateButton.Label = "Dissociate";
            this.DissociateButton.Name = "DissociateButton";
            this.DissociateButton.OfficeImageId = "PrintPreviewClose";
            this.DissociateButton.ShowImage = true;
            this.DissociateButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.DissociateButton_Click);
            // 
            // RefreshBtn
            // 
            this.RefreshBtn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.RefreshBtn.Label = "Refresh";
            this.RefreshBtn.Name = "RefreshBtn";
            this.RefreshBtn.OfficeImageId = "Refresh";
            this.RefreshBtn.ShowImage = true;
            this.RefreshBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.RefreshBtn_Click);
            // 
            // UpdateAllButton
            // 
            this.UpdateAllButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.UpdateAllButton.Label = "Update All";
            this.UpdateAllButton.Name = "UpdateAllButton";
            this.UpdateAllButton.OfficeImageId = "UpgradeDocument";
            this.UpdateAllButton.ShowImage = true;
            this.UpdateAllButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.UpdateAllButton_Click);
            // 
            // UpdateWorksheet
            // 
            this.UpdateWorksheet.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.UpdateWorksheet.Label = "Update Worksheet";
            this.UpdateWorksheet.Name = "UpdateWorksheet";
            this.UpdateWorksheet.OfficeImageId = "UpgradeDocument";
            this.UpdateWorksheet.ShowImage = true;
            this.UpdateWorksheet.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.UpdateWorksheet_Click);
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            // 
            // CreateInfoPointButton
            // 
            this.CreateInfoPointButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.CreateInfoPointButton.Label = "Create InfoPoint";
            this.CreateInfoPointButton.Name = "CreateInfoPointButton";
            this.CreateInfoPointButton.OfficeImageId = "CreateHandoutsInWord";
            this.CreateInfoPointButton.ShowImage = true;
            this.CreateInfoPointButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.CreateInfoPointButton_Click);
            // 
            // ListInfoPoints
            // 
            this.ListInfoPoints.Label = "List of Info Points";
            this.ListInfoPoints.Name = "ListInfoPoints";
            // 
            // separator3
            // 
            this.separator3.Name = "separator3";
            // 
            // CreateTablePicButton
            // 
            this.CreateTablePicButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.CreateTablePicButton.ImageName = "CreateTable";
            this.CreateTablePicButton.Label = "Create TablePic";
            this.CreateTablePicButton.Name = "CreateTablePicButton";
            this.CreateTablePicButton.OfficeImageId = "CreateTable";
            this.CreateTablePicButton.ShowImage = true;
            this.CreateTablePicButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.CreateTablePicButton_Click);
            // 
            // ListTablePics
            // 
            this.ListTablePics.Label = "List of TablePics";
            this.ListTablePics.Name = "ListTablePics";
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            // 
            // Associatedfile
            // 
            this.Associatedfile.Label = "Associated file :";
            this.Associatedfile.Name = "Associatedfile";
            this.Associatedfile.SizeString = "1111111111111111111111111111111111111";
            this.Associatedfile.Text = null;
            // 
            // ExcelRibbon
            // 
            this.Name = "ExcelRibbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.DATA_BRIDGE);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.ExcelRibbon_Load);
            this.DATA_BRIDGE.ResumeLayout(false);
            this.DATA_BRIDGE.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab DATA_BRIDGE;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AssociateButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton DissociateButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton UpdateAllButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton UpdateWorksheet;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton CreateTablePicButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonDropDown ListTablePics;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton CreateInfoPointButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonDropDown ListInfoPoints;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator3;
        public Microsoft.Office.Tools.Ribbon.RibbonEditBox Associatedfile;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton RefreshBtn;
    }

    partial class ThisRibbonCollection
    {
        internal ExcelRibbon ExcelRibbon
        {
            get { return this.GetRibbon<ExcelRibbon>(); }
        }
    }
}
