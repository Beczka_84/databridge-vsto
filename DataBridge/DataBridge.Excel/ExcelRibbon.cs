﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using DataBridge.Api.Service;
using DataBridge.Api.Model;
using System.Windows.Forms;
using DataBridge.Api.DAL;
using Microsoft.Office.Interop.Excel;
using System.IO;
using DataBridge.Excel.Services;
using WordApp = Microsoft.Office.Interop.Word;
using System.Drawing;
using System.Threading.Tasks;
using Microsoft.Office.Core;
using Constants = DataBridge.Api.Model.Constants;

namespace DataBridge.Excel
{
    public partial class ExcelRibbon
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private void ExcelRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            this.ListTablePics.SelectionChanged += ListTablePics_SelectionChanged;
            this.ListInfoPoints.SelectionChanged += ListInfoPoints_SelectionChanged;
           // HookManager.SubscribeToWindowEvents();

        }


        private void worksheetsDropdown_SelectionChanged(object sender, RibbonControlEventArgs e)
        {
            //((Worksheet) Globals.ThisAddIn.Application.ActiveWorkbook.Sheets[this.worksheetsDropdown.SelectedItem.Label]).Select();
        }


        private void ListInfoPoints_SelectionChanged(object sender, RibbonControlEventArgs e)
        {
            foreach (Name excelName in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
            {
                if (excelName.Name == this.ListInfoPoints.SelectedItem.Label)
                {
                    Range rng = excelName.RefersToRange;
                    Worksheet sht = (Worksheet)Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets[rng.Worksheet.Name.ToString()];
                    sht.Select();
                    rng.Select();
                }
            }
        }

        private void ListTablePics_SelectionChanged(object sender, RibbonControlEventArgs e)
        {
            foreach (Name excelName in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
            {
                if (excelName.Name == this.ListTablePics.SelectedItem.Label)
                {
                    Range rng = excelName.RefersToRange;
                    Worksheet sht = (Worksheet)Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets[rng.Worksheet.Name.ToString()];
                    sht.Select();
                    rng.Select();
                }
            }
        }

        private void AssociateButton_Click(object sender, RibbonControlEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Filter = "Word Files | *.doc; *.docx; *.docm";
            DialogResult result = dialog.ShowDialog();


            if (result == DialogResult.Cancel)
            {
                MessageBox.Show("You canceled the file open dialog");
            }

            if (result == DialogResult.OK)
            {
                WordVM wordVM = new WordVM();
                wordVM.Path = System.IO.Path.GetDirectoryName(dialog.FileName);
                wordVM.Name = System.IO.Path.GetFileName(dialog.FileName);

                ExcelVM excelVM = new ExcelVM();
                excelVM.Path = Globals.ThisAddIn.Application.ActiveWorkbook.Path;
                excelVM.Name = Globals.ThisAddIn.Application.ActiveWorkbook.Name;
                ExcelService excelService = new ExcelService();
                ServiceResult serviceResult = excelService.AssociateWord(wordVM, excelVM);

                if (serviceResult.Sucess)
                {
                    WordService wordService = new WordService();

                    List<string> namesList = new List<string>();
                    foreach (Name excelName in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
                    {
                        if (excelName.Name.Contains(Api.Model.Constants.IPPrefix) || excelName.Name.Contains(Api.Model.Constants.TPPrefix))
                        {
                            namesList.Add(excelName.Name);
                        }
                    }

                    Globals.ThisAddIn.Application.StatusBar = "Opening word ...";
                    RefreshWordDtaTables(excelVM, wordService, wordVM, namesList);

                    Globals.Ribbons.ExcelRibbon.AssociateButton.Enabled = false;
                    Globals.Ribbons.ExcelRibbon.DissociateButton.Enabled = true;
                    Globals.Ribbons.ExcelRibbon.Associatedfile.Text = wordVM.Name;
                }
                else
                {
                    MessageBox.Show(string.Format("Error : {0}", serviceResult.ErrorMessage), "DataBridge.Excel");
                }

            }

        }

        private void DissociateButton_Click(object sender, RibbonControlEventArgs e)
        {
            ExcelVM excelVM = new ExcelVM();
            excelVM.Path = Globals.ThisAddIn.Application.ActiveWorkbook.Path;
            excelVM.Name = Globals.ThisAddIn.Application.ActiveWorkbook.Name;
            ExcelService excelService = new ExcelService();

            Word AssociatedWord = excelService.GetAssociatedWord(excelVM);

            if (AssociatedWord == null)
            {
                MessageBox.Show(string.Format("No Word File Associated to this Excel", "DataBridge.Excel"));
            }

            WordVM wordVM = new WordVM();
            wordVM.Path = AssociatedWord.Path;
            wordVM.Name = AssociatedWord.Name;

            ServiceResult serviceResult = excelService.DissociateWord(wordVM, excelVM);

            if (serviceResult.Sucess)
            {

                Globals.Ribbons.ExcelRibbon.AssociateButton.Enabled = true;
                Globals.Ribbons.ExcelRibbon.DissociateButton.Enabled = false;
                Globals.Ribbons.ExcelRibbon.Associatedfile.Text = "";
            }
            else
            {
                MessageBox.Show(string.Format("Error : {0}", serviceResult.ErrorMessage), "DataBridge.Excel");
            }
        }

        private void CreateTablePicButton_Click(object sender, RibbonControlEventArgs e)
        {
            bool create = true;

            string name = (string)Globals.ThisAddIn.Application.InputBox("Please insert name for selected Table Pic");
            name = string.Join("", Api.Model.Constants.TPPrefix, name);
            if (!CheckUniqueName(name))
            {
                DialogResult result = MessageBox.Show("Do you want to override existing table pic with this name?", "Table Pic name conflict", MessageBoxButtons.YesNoCancel);

                switch (result)
                {
                    case DialogResult.Yes:
                        create = true;
                        DeleteName(name);
                        break;
                    case DialogResult.No:
                        create = false;
                        break;
                    case DialogResult.Cancel:
                        create = false;
                        break;
                }
            }

            if (create)
            {
                Range rng = (Range)Globals.ThisAddIn.Application.Selection;
                try
                {
                    Name newName = Globals.ThisAddIn.Application.ActiveWorkbook.Names.Add(name, rng);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Error : {0}", ex.Message), "DataBridge.Excel");
                    return;
                }

               
                ExcelVM excelVM = new ExcelVM();
                excelVM.Path = Globals.ThisAddIn.Application.ActiveWorkbook.Path;
                excelVM.Name = Globals.ThisAddIn.Application.ActiveWorkbook.Name;

                DataTableVM datTableVM = new DataTableVM();

                RangeToImageService rangeToImageService = new RangeToImageService();

                byte[] imageByteArray = rangeToImageService.GetImageBytes(rng);

                if (!imageByteArray.Any())
                {
                    MessageBox.Show(string.Format("Error saving image to database try again"), "DataBridge.Excel");
                    return;
                }

                datTableVM.Name = name;
                datTableVM.Type = DataType.InfoTable;
                datTableVM.Image = imageByteArray;

                ExcelService excelService = new ExcelService();
                ServiceResult serviceResult = excelService.CreateDataTable(excelVM, datTableVM);

                if (serviceResult.Sucess)
                {

                    this.ListTablePics.Items.Clear();

                    RibbonDropDownItem emptyTablePic = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                    emptyTablePic.Label = Api.Model.Constants.Blank;
                    Globals.Ribbons.ExcelRibbon.ListTablePics.Items.Add(emptyTablePic);

                    List<Api.DAL.DataTable> dataTableList = excelService.GetDataTables(excelVM).ToList();
                    foreach (Api.DAL.DataTable item in dataTableList.Where(x => x.Type == DataType.InfoTable))
                    {
                        RibbonDropDownItem newTablePic = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                        newTablePic.Label = item.Name;
                        newTablePic.OfficeImageId = "CreateHandoutsInWord";
                        this.ListTablePics.Items.Add(newTablePic);
                    }
                }
                else
                {
                    foreach (Name excelName in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
                    {
                        if (excelName.Name == name)
                        {
                            excelName.Delete();
                        }
                    }
                    MessageBox.Show(string.Format("Error : {0}", serviceResult.ErrorMessage), "DataBridge.Excel");
                }
            }
        }

        private bool CheckUniqueName(string name)
        {
            foreach (Name excelName in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
            {
                if (excelName.Name == name) return false;
            }
            return true;
        }

        private void DeleteName(string name)
        {
            foreach (Name excelName in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
            {
                if (excelName.Name == name)
                {
                    excelName.Delete();
                    return;
                }
            }
            return;
        }

        private void CreateInfoPointButton_Click(object sender, RibbonControlEventArgs e)
        {
            bool create = true;

            Range rng = (Range)Globals.ThisAddIn.Application.Selection;
            if (rng.Cells.Count > 1)
            {
                MessageBox.Show(string.Format("Please select a single cell", "DataBridge.Excel"));
                return;
            }

            string name = (string)Globals.ThisAddIn.Application.InputBox("Please insert name for selected Info Point");
            name = string.Join("", Api.Model.Constants.IPPrefix, name);
            if (!CheckUniqueName(name))
            {
                DialogResult result = MessageBox.Show("Do you want to override existing info point with this name?", "Info Poin name conflict", MessageBoxButtons.YesNoCancel);

                switch (result)
                {
                    case DialogResult.Yes:
                        create = true;
                        DeleteName(name);
                        break;
                    case DialogResult.No:
                        create = false;
                        break;
                    case DialogResult.Cancel:
                        create = false;
                        break;
                }
            }

            if (create)
            {
                try
                {
                    Name newName = Globals.ThisAddIn.Application.ActiveWorkbook.Names.Add(name, rng);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Error : {0}", ex.Message), "DataBridge.Excel");
                    return;
                }

                ExcelVM excelVM = new ExcelVM();
                excelVM.Path = Globals.ThisAddIn.Application.ActiveWorkbook.Path;
                excelVM.Name = Globals.ThisAddIn.Application.ActiveWorkbook.Name;

                DataTableVM datTableVM = new DataTableVM();
                datTableVM.Name = name;
                datTableVM.Type = DataType.InfoPoint;
                Range rngValue = rng.Cells[1, 1];
                datTableVM.Value = rngValue.Text;

                ExcelService excelService = new ExcelService();
                ServiceResult serviceResult = excelService.CreateDataTable(excelVM, datTableVM);

                if (serviceResult.Sucess)
                {

                    this.ListInfoPoints.Items.Clear();

                    RibbonDropDownItem emptyTablePic = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                    emptyTablePic.Label = Api.Model.Constants.Blank;
                    Globals.Ribbons.ExcelRibbon.ListInfoPoints.Items.Add(emptyTablePic);

                    List<Api.DAL.DataTable> dataTableList = excelService.GetDataTables(excelVM).ToList();
                    foreach (Api.DAL.DataTable item in dataTableList.Where(x => x.Type == DataType.InfoPoint))
                    {
                        RibbonDropDownItem newTablePic = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                        newTablePic.Label = item.Name;
                        newTablePic.OfficeImageId = "CreateHandoutsInWord";
                        this.ListInfoPoints.Items.Add(newTablePic);
                    }
                }
                else
                {
                    foreach (Name excelName in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
                    {
                        if (excelName.Name == name)
                        {
                            excelName.Delete();
                        }
                    }
                    MessageBox.Show(string.Format("Error : {0}", serviceResult.ErrorMessage), "DataBridge.Excel");
                }
            }
        }


        private ServiceResult RefreshWorkbook()
        {
            ExcelVM excelVM = new ExcelVM();
            excelVM.Path = Globals.ThisAddIn.Application.ActiveWorkbook.Path;
            excelVM.Name = Globals.ThisAddIn.Application.ActiveWorkbook.Name;

            ExcelService excelService = new ExcelService();
            RangeToImageService rangeToImageService = new RangeToImageService();
            List<DataTableVM> listOfDataPoints = new List<DataTableVM>();

            Globals.ThisAddIn.Application.Visible = false;
            RefreshWrbProgress progress = new RefreshWrbProgress();
            progress.Show();


            foreach (Name excelName in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
            {

                try
                {
                    if (excelName.Name.Contains(Constants.TPPrefix))
                    {
                        DataTableVM datTableVM = new DataTableVM();
                        datTableVM.Name = excelName.Name;
                        datTableVM.Type = DataType.InfoTable;
                        Range rng = excelName.RefersToRange;
                        byte[] imageByteArray = rangeToImageService.GetImageBytes(rng);

                        if (imageByteArray.Any())
                        {
                            datTableVM.Image = imageByteArray;
                            listOfDataPoints.Add(datTableVM);
                        }
                    }

                    if (excelName.Name.Contains(Constants.IPPrefix))
                    {
                        DataTableVM datTableVM = new DataTableVM();
                        datTableVM.Name = excelName.Name;
                        datTableVM.Type = DataType.InfoPoint;
                        Range rng = excelName.RefersToRange;
                        Range rngValue = rng.Cells[1, 1];
                        datTableVM.Value = rngValue.Text;
                        listOfDataPoints.Add(datTableVM);
                    }
                }
                catch (Exception e)
                {
                    log.Error(e);
                }
            }

            Globals.ThisAddIn.Application.Visible = true;
            progress.Close();
            Globals.ThisAddIn.Application.ScreenUpdating = true;
            ServiceResult serviceResult = excelService.UpdateDataTables(excelVM, listOfDataPoints);

            if (serviceResult.Sucess)
            {
                this.ListTablePics.Items.Clear();
                this.ListInfoPoints.Items.Clear();

                List<Api.DAL.DataTable> dataTableList = excelService.GetDataTables(excelVM).ToList();

                RibbonDropDownItem emptyTablePic = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                emptyTablePic.Label = Api.Model.Constants.Blank;
                Globals.Ribbons.ExcelRibbon.ListTablePics.Items.Add(emptyTablePic);

                RibbonDropDownItem emptyTablePic2 = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                emptyTablePic2.Label = Api.Model.Constants.Blank;
                Globals.Ribbons.ExcelRibbon.ListInfoPoints.Items.Add(emptyTablePic2);

                foreach (Api.DAL.DataTable item in dataTableList.Where(x => x.Type == DataType.InfoTable))
                {
                    RibbonDropDownItem newTablePic = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                    newTablePic.Label = item.Name;
                    newTablePic.OfficeImageId = "CreateHandoutsInWord";
                    this.ListTablePics.Items.Add(newTablePic);
                }

                foreach (Api.DAL.DataTable item in dataTableList.Where(x => x.Type == DataType.InfoPoint))
                {
                    RibbonDropDownItem newInfoPoint = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                    newInfoPoint.Label = item.Name;
                    newInfoPoint.OfficeImageId = "CreateHandoutsInWord";
                    this.ListInfoPoints.Items.Add(newInfoPoint);
                }

                ServiceResult clearResult = excelService.ClearDB();
                if (!clearResult.Sucess)
                {
                    log.Error(clearResult.ErrorMessage);
                }

                return new ServiceResult() { Sucess = true, ErrorMessage = "" };
            }
            else
            {
                MessageBox.Show(string.Format("Error : {0}", serviceResult.ErrorMessage), "DataBridge.Excel");
                return new ServiceResult() { Sucess = false, ErrorMessage = serviceResult.ErrorMessage };
            }
        }

        private void UpdateAllButton_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.Application.StatusBar = "Refreshing database ...";
            ServiceResult serviceResult = RefreshWorkbook();
            if (serviceResult.Sucess)
            {

                ExcelVM excelVM = new ExcelVM();
                excelVM.Path = Globals.ThisAddIn.Application.ActiveWorkbook.Path;
                excelVM.Name = Globals.ThisAddIn.Application.ActiveWorkbook.Name;

                ExcelService excelService = new ExcelService();
                WordService wordService = new WordService();

                Word AssociatedWord = excelService.GetAssociatedWord(excelVM);

                if (AssociatedWord == null)
                {
                    MessageBox.Show(string.Format("No Word File Associated to this Excel", "DataBridge.Excel"));
                    return;

                }

                WordVM wordVM = new WordVM();
                wordVM.Path = AssociatedWord.Path;
                wordVM.Name = AssociatedWord.Name;

                List<string> namesList = new List<string>();
                foreach (Name excelName in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
                {
                    if (excelName.Name.Contains(Api.Model.Constants.IPPrefix) || excelName.Name.Contains(Api.Model.Constants.TPPrefix))
                    {
                        namesList.Add(excelName.Name);
                    }
                }
                Globals.ThisAddIn.Application.StatusBar = "Opening word ...";
                //Task.Run(() => UopdateWordDtaTables(excelVM, wordService, AssociatedWord, wordVM, namesList));
                UopdateWordDtaTables(excelVM, wordService, AssociatedWord, wordVM, namesList);

                Globals.ThisAddIn.Application.StatusBar = "Done ...";
                Globals.ThisAddIn.Application.StatusBar = false;

            }
            else
            {
                MessageBox.Show(string.Format("Error : {0}", serviceResult.ErrorMessage), "DataBridge.Excel");
                Globals.ThisAddIn.Application.StatusBar = false;
            }
        }

        private void UpdateWorksheet_Click(object sender, RibbonControlEventArgs e)
        {
            Globals.ThisAddIn.Application.StatusBar = "Refreshing database ...";
            ServiceResult serviceResult = RefreshWorkbook();
            if (serviceResult.Sucess)
            {

                ExcelVM excelVM = new ExcelVM();
                excelVM.Path = Globals.ThisAddIn.Application.ActiveWorkbook.Path;
                excelVM.Name = Globals.ThisAddIn.Application.ActiveWorkbook.Name;

                ExcelService excelService = new ExcelService();
                WordService wordService = new WordService();


                Word AssociatedWord = excelService.GetAssociatedWord(excelVM);

                if (AssociatedWord == null)
                {
                    MessageBox.Show(string.Format("No Word File Associated to this Excel", "DataBridge.Excel"));
                }

                WordVM wordVM = new WordVM();
                wordVM.Path = AssociatedWord.Path;
                wordVM.Name = AssociatedWord.Name;

                List<string> namesList = new List<string>();
                foreach (Name excelName in Globals.ThisAddIn.Application.ActiveWorkbook.Names)
                {
                    if (excelName.Name.Contains(Api.Model.Constants.IPPrefix) || excelName.Name.Contains(Api.Model.Constants.TPPrefix))
                    {
                        try
                        {
                            Range rng = excelName.RefersToRange;
                            Worksheet sht = (Worksheet)Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets[rng.Worksheet.Name.ToString()];
                            if (sht.Name == Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet.Name)
                            {
                                namesList.Add(excelName.Name);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex);
                        }
                       
                    }
                }
                Globals.ThisAddIn.Application.StatusBar = "Opening word ...";
                //Task.Run(() => UopdateWordDtaTables(excelVM, wordService, AssociatedWord, wordVM, namesList));
                UopdateWordDtaTables(excelVM, wordService, AssociatedWord, wordVM, namesList);

                Globals.ThisAddIn.Application.StatusBar = "Done ...";
                Globals.ThisAddIn.Application.StatusBar = false;
            }
            else
            {
                MessageBox.Show(string.Format("Error : {0}", serviceResult.ErrorMessage), "DataBridge.Excel");
                Globals.ThisAddIn.Application.StatusBar = false;
            }
        }

        private void UopdateWordDtaTables(ExcelVM excelVM, WordService wordService, Word AssociatedWord, WordVM wordVM, List<string> namesList)
        {
            ProgressUpdate pro = new ProgressUpdate(excelVM, wordService, AssociatedWord, wordVM, namesList);
        }


        private void RefreshWordDtaTables(ExcelVM excelVM, WordService wordService, WordVM wordVM, List<string> namesList)
        {
            ProgressScanCC pro = new ProgressScanCC(excelVM, wordService, wordVM, namesList);
        }

        private void RefreshBtn_Click(object sender, RibbonControlEventArgs e)
        {
            ServiceResult serviceResult = RefreshWorkbook();
            if (!serviceResult.Sucess)
            {
                MessageBox.Show(string.Format("Error : {0}", serviceResult.ErrorMessage), "DataBridge.Excel");
            }
        }

        private void RandomizeData_Click(object sender, RibbonControlEventArgs e)
        {
            MessageBox.Show("Not implemented yet", "DataBridge.Excel");
        }

        private void PictureThis_Click_1(object sender, RibbonControlEventArgs e)
        {
            Range rng = (Range)Globals.ThisAddIn.Application.Selection;
            RangeToImageService rangeToImageService = new RangeToImageService();

            Image imageToSave = rangeToImageService.GetImage(rng);

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            //saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            saveFileDialog1.Filter = "Jpg files (*.JPG)|*.jpg|All Files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                imageToSave.Save(saveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
        }
    }
}
