﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataBridge.Api.DAL;
using DataBridge.Api.Model;
using DataBridge.Api.Service;
using DataBridge.Excel.Services;
using ImageMagick;
using Microsoft.Office.Core;
using WordApp = Microsoft.Office.Interop.Word;

namespace DataBridge.Excel
{
    public partial class ProgressUpdate : Form
    {
        private ExcelVM _excelVM { get; set; }
        private WordService _wordService { get; set; }
        private Word _associatedWord { get; set; }
        private WordVM _wordVM { get; set; }
        private List<string> _namesList { get; set; }

        public ProgressUpdate(ExcelVM excelVM, WordService wordService, Word AssociatedWord, WordVM wordVM, List<string> namesList)
        {
            InitializeComponent();

            this._namesList = namesList;
            this._associatedWord = AssociatedWord;
            this._excelVM = excelVM;
            this._wordService = wordService;
            this._wordVM = wordVM;

            this.Show();
        }

        private void Progress_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Globals.ThisAddIn.Application.Visible = false;
            string[] progIds = { "Word.Application" };
            WordApp.Application app = null;
            WordApp.Document doc = null;
            try
            {
                WordInstances wordInstances = new WordInstances();
                List<object> instances = wordInstances.GetRunningInstances(progIds);


                foreach (WordApp.Application wordApp in instances)
                {
                    app = wordApp;
                    foreach (WordApp.Document singleDoc in app.Documents)
                    {
                        if (singleDoc.Name == Path.GetFileName(_associatedWord.FullPath))
                        {
                            this.label1.Text = "Attaching to open word document ....";
                            doc = singleDoc;
                            break;
                        }
                    }
                }
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                if (File.Exists(_associatedWord.FullPath))
                {
                    this.label1.Text = "Opening word document ....";
                    app = new WordApp.Application();
                    doc = app.Documents.Open(_associatedWord.FullPath);
                }
            }

            if (doc == null)
            {
                if (File.Exists(_associatedWord.FullPath))
                {
                    this.label1.Text = "Opening word document ....";
                    app = new WordApp.Application();
                    doc = app.Documents.Open(_associatedWord.FullPath);
                }
                else
                {
                    MessageBox.Show(string.Format("File : {0} don't exist", _associatedWord.FullPath), "DataBridge.Excel");
                    return;
                }
            }

            try
            {
                app.Visible = false;
                app.ScreenUpdating = false;
                IEnumerable<Api.DAL.DataTable> dataTables = _wordService.GetDataTables(_wordVM, _excelVM);
                Api.DAL.Excel excel = _wordService.GetAssociatedExcels(_wordVM).FirstOrDefault(x => x.FullPath == _excelVM.FullPath);

                if (excel == null)
                {
                    Globals.ThisAddIn.Application.Visible = true;
                    this.Close();
                }

                this.label1.Text = "Starting ....";
                this.progressBar1.Maximum = doc.ContentControls.Count;

                for (int i = 0; i < doc.ContentControls.Count; i++)
                {
                    this.label1.Text = string.Format("Updating word content controls {0} of {1}", i+1, doc.ContentControls.Count);
                    this.progressBar1.Value = i + 1;

                    Microsoft.Office.Interop.Word.ContentControl item = doc.ContentControls[i + 1];

                    if (_namesList.Contains(item.Title) && Convert.ToInt16(item.Tag) == excel.Id)
                    {
                        if (item.Title.Contains(Constants.IPPrefix)) //WordApp.WdContentControlType.wdContentControlRichText)
                        {
                            string value = dataTables.FirstOrDefault(x => x.Name == item.Title && x.Type == DataType.InfoPoint)?.Value ?? string.Empty;

                            if (value == null || value == string.Empty)
                            {
                                item.Range.Text = item.Title;
                                item.Range.HighlightColorIndex = WordApp.WdColorIndex.wdYellow;
                            }
                            else
                            {
                                item.Range.Text = value;
                            }
                        }

                        if (item.Title.Contains(Constants.TPPrefix)) // == WordApp.WdContentControlType.wdContentControlPicture)
                        {
                            RangeToImageService rangeToImageService = new RangeToImageService();
                            Api.DAL.DataTable dataTable = dataTables.FirstOrDefault(x => x.Name == item.Title && x.Type == DataType.InfoTable);
                            MagickImage image = rangeToImageService.GetImageFromBytes(dataTable.Image);

                            if (image != null)
                            {
                                WordApp.WdWrapType wrapp = WordApp.WdWrapType.wdWrapSquare;
                                float Height = 0;
                                float Width = 0;
                                string path = string.Join(@"\", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "temp.png");
                                var pagesetup = app.ActiveDocument.PageSetup;
                                MagickImage image2 = rangeToImageService.ResizeImage(image, pagesetup);
                                image2.Write(path);

                                if (item.Range.ShapeRange.Count > 0)
                                {
                                    wrapp = item.Range.ShapeRange[1].WrapFormat.Type;
                                    Height = image.Height; //item.Range.ShapeRange[1].Height;
                                    Width = item.Range.ShapeRange[1].Width;
                                    item.Range.ShapeRange[1].Delete();
                                }


                                if (item.Range.InlineShapes.Count > 0)
                                {
                                    //Height = image.Height; // item.Range.InlineShapes[1].Height;
                                    Width = item.Range.InlineShapes[1].Width;
                                    item.Range.InlineShapes[1].Delete();
                                }

                                item.SetPlaceholderText(null, null, string.Empty);
                                var ilineshape = item.Range.InlineShapes.AddPicture(path);
                                var shape = ilineshape.ConvertToShape();
                                if (wrapp != WordApp.WdWrapType.wdWrapInline)
                                {
                                    shape.WrapFormat.Type = WordApp.WdWrapType.wdWrapInline; // wrapp;
                                }
                                else
                                {
                                    shape.WrapFormat.Type = WordApp.WdWrapType.wdWrapInline;
                                }

                                if (Height != 0 && Width != 0)
                                {
                                    shape.WrapFormat.Type = WordApp.WdWrapType.wdWrapInline; //wrapp;
                                    shape.Width = Width;
                                    shape.Height = Height;
                                }

                                shape.LockAspectRatio = MsoTriState.msoCTrue;
                                System.IO.File.Delete(path);

                            }
                        }
                    }
                }
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error : {0}", ex.Message), "DataBridge.Excel");
                doc.Close();
                app.Quit();
                Globals.ThisAddIn.Application.Visible = true;
                this.Close();
            }
            app.ScreenUpdating = true;
            app.Visible = true;
            Globals.ThisAddIn.Application.Visible = true;
            this.Close();
        }
    }
}
