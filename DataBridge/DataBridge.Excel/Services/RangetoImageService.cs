﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ImageMagick;

namespace DataBridge.Excel.Services
{
    public class RangeToImageService
    {
        public byte[] GetImageBytes(Range rng)
        {
            try
            {
                Range temRange = rng;
                rng.Copy();
                temRange.PasteSpecial(XlPasteType.xlPasteValues);
                temRange.CopyPicture(XlPictureAppearance.xlScreen, XlCopyPictureFormat.xlBitmap);
                //rng.SpecialCells(XlCellType.xlCellTypeVisible).CopyPicture(XlPictureAppearance.xlScreen, XlCopyPictureFormat.xlBitmap);
                byte[] data;
                System.Drawing.Image Image = Clipboard.GetImage();
                using (MagickImage image = new MagickImage(new Bitmap(Image)))
                {
                    image.Format = MagickFormat.Png;
                    data = image.ToByteArray();
                }

                //MemoryStream ms = new MemoryStream();
                //Image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                //return ms.ToArray();
                return data;
            }
            catch (Exception)
            {

                throw;
            }
         
        }

        public MagickImage ResizeImage(MagickImage magicImage, Microsoft.Office.Interop.Word.PageSetup pageSetup)
        {
            try
            {
                var reservedForTitle = CentimeterToPixel(1.27);
                var docWidth = CentimeterToPixel((((pageSetup.PageWidth - (pageSetup.LeftMargin + pageSetup.RightMargin)) / 72) * 2.54d));
                var docHeight = CentimeterToPixel((((pageSetup.PageHeight - (pageSetup.TopMargin + pageSetup.BottomMargin)) / 72) * 2.54d)) - reservedForTitle;
               
               
                MagickImage ImagickImagemage;
                using (MagickImage image = new MagickImage(magicImage))
                {
                    if (image.Width > docWidth && image.Height < docHeight)
                    {
                        MagickGeometry size = new MagickGeometry( (int)docWidth, (int)docHeight);
                        size.IgnoreAspectRatio = false;
                        image.Resize(size);
                        ImagickImagemage = image.Clone();
                    }
                    else if(image.Width < docWidth && image.Height > docHeight)
                    {
                        MagickGeometry size = new MagickGeometry((int)image.Width, (int)docHeight);
                        size.IgnoreAspectRatio = false;
                        image.Resize(size);
                        ImagickImagemage = image.Clone();
                    }
                    else if (image.Width > docWidth && image.Height > docHeight)
                    {
                        MagickGeometry size = new MagickGeometry((int)docWidth, (int)docHeight);
                        size.IgnoreAspectRatio = false;
                        image.Resize(size);
                        ImagickImagemage = image.Clone();
                    }
                    else
                    {
                        ImagickImagemage = image.Clone();
                    }
                }
                return ImagickImagemage;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        int CentimeterToPixel(double Centimeter)
        {
            double pixel = -1;
            using (Graphics g = Graphics.FromHwnd(IntPtr.Zero))
            {
                //dpiX = graphics.DpiX;
                //dpiY = graphics.DpiY;

                pixel = Centimeter * g.DpiY / 2.54d;
            }
            return (int)pixel;
        }

        public Image GetImage(Range rng)
        {
            try
            {
                Range temRange = rng;
                rng.Copy();
                temRange.PasteSpecial(XlPasteType.xlPasteValues);
                temRange.CopyPicture(XlPictureAppearance.xlScreen, XlCopyPictureFormat.xlBitmap);
                System.Drawing.Image Image = Clipboard.GetImage();
                return Image;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public MagickImage GetImageFromBytes(byte[] byteArrayIn)
        {
            try
            {
                MagickImage ImagickImagemage;
                using (MagickImage image = new MagickImage(byteArrayIn))
                {
                    image.Format = MagickFormat.Png;
                    ImagickImagemage = image.Clone();
                }
                return ImagickImagemage;

                //MemoryStream ms = new MemoryStream(byteArrayIn);
                //Image returnImage = Image.FromStream(ms);
                //return returnImage;
            }
            catch (Exception e)
            {
                return null;
            }

        }
    }
}
