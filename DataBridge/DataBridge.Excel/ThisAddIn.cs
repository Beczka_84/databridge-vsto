﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using DataBridge.Api.DAL;
using System.Windows.Forms;
using Microsoft.Office.Tools.Ribbon;
using DataBridge.Api.Service;
using DataBridge.Api.Model;
using Microsoft.Office.Interop.Excel;
using Constants = DataBridge.Api.Model.Constants;

namespace DataBridge.Excel
{

    public partial class ThisAddIn
    {
        private IntPtr windowEventHook;
        private delegate void WinEventProc(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime);
        private WinEventProc winEventProc;

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr SetWinEventHook(int eventMin, int eventMax, IntPtr hmodWinEventProc, WinEventProc lpfnWinEventProc, int idProcess, int idThread, int dwflags);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int UnhookWinEvent(IntPtr hWinEventHook);

        private const int WINEVENT_INCONTEXT = 4;
        private const int WINEVENT_OUTOFCONTEXT = 0;
        private const int WINEVENT_SKIPOWNPROCESS = 2;
        private const int WINEVENT_SKIPOWNTHREAD = 1;

        private const int EVENT_SYSTEM_FOREGROUND = 3;

        public void SubscribeToWindowEvents()
        {
            this.winEventProc = WindowEventCallback;
            if (windowEventHook == IntPtr.Zero)
            {
                windowEventHook = SetWinEventHook(
                    EVENT_SYSTEM_FOREGROUND, // eventMin
                    EVENT_SYSTEM_FOREGROUND, // eventMax
                    IntPtr.Zero, // hmodWinEventProc
                    this.winEventProc, // lpfnWinEventProc
                    0, // idProcess
                    0, // idThread
                    WINEVENT_OUTOFCONTEXT);
            }
        }

        void WindowEventCallback(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime)
        {
            if (eventType == 3)
            {
                if (hwnd == (IntPtr) Globals.ThisAddIn.Application.Hwnd)
                {
                    Refreshribbon();
                    RefreshSheets();
                    //MessageBox.Show(hwnd.ToString());
                }
            }
        }

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            this.Application.WorkbookActivate += Application_WorkbookActivate;
            this.Application.WindowDeactivate += Application_WindowDeactivate;
            this.Application.WindowActivate += Application_WindowActivate;
            this.Application.WorkbookNewSheet += Application_WorkbookNewSheet;
            this.Application.WorkbookOpen += Application_WorkbookOpen;
            SubscribeToWindowEvents();
            RefreshSheets();
        }

        private void Application_WorkbookOpen(Workbook Wb)
        {
            RefreshSheets();
        }

        private void Application_WorkbookNewSheet(Workbook Wb, object Sh)
        {
            RefreshSheets();
        }

        private void Application_WindowDeactivate(Microsoft.Office.Interop.Excel.Workbook Wb, Microsoft.Office.Interop.Excel.Window Wn)
        {
         
        }

        private void Application_WindowActivate(Microsoft.Office.Interop.Excel.Workbook Wb, Microsoft.Office.Interop.Excel.Window Wn)
        {
             Refreshribbon();

        }

        private void Application_WorkbookActivate(Microsoft.Office.Interop.Excel.Workbook Wb)
        {
            Refreshribbon();

        }

        private static void RefreshSheets()
        {
            if (Globals.ThisAddIn.Application.ActiveWorkbook != null)
            {
                //if (Globals.Ribbons.ExcelRibbon.worksheetsDropdown.Items.Count != Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.Count)
                //{
                //    Globals.Ribbons.ExcelRibbon.worksheetsDropdown.Items.Clear();

                //    foreach (Worksheet worksheet in Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets)
                //    {
                //        RibbonDropDownItem wrk = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                //        wrk.Label = worksheet.Name;
                //    }
                //}
            }
        }

        private static void Refreshribbon()
        {
            if (Globals.ThisAddIn.Application.Workbooks.Count > 0)
            {
                Globals.Ribbons.ExcelRibbon.AssociateButton.Enabled = true;
                Globals.Ribbons.ExcelRibbon.DissociateButton.Enabled = false;
                Globals.Ribbons.ExcelRibbon.Associatedfile.Text = "";

                ExcelService excelService = new ExcelService();
                ExcelVM excelVM = new ExcelVM();

                if (Path.GetExtension(Globals.ThisAddIn.Application.ActiveWorkbook.Name) == ".xls")
                {
                    return;
                };

                string path = Globals.ThisAddIn.Application.ActiveWorkbook.Path;

                if (path == "")
                {
                    return;
                }

                excelVM.Path = path;
                excelVM.Name = Globals.ThisAddIn.Application.ActiveWorkbook.Name;

                Word AssociatedWord = excelService.GetAssociatedWord(excelVM);

                if (AssociatedWord != null)
                {
                    Globals.Ribbons.ExcelRibbon.AssociateButton.Enabled = false;
                    Globals.Ribbons.ExcelRibbon.DissociateButton.Enabled = true;
                    Globals.Ribbons.ExcelRibbon.Associatedfile.Text = AssociatedWord.Name;
                }

                Globals.Ribbons.ExcelRibbon.ListTablePics.Items.Clear();
                Globals.Ribbons.ExcelRibbon.ListInfoPoints.Items.Clear();
             

                List<Api.DAL.DataTable> dataTableList = excelService.GetDataTables(excelVM).ToList();

                RibbonDropDownItem emptyTablePic = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                emptyTablePic.Label = Constants.Blank;
                Globals.Ribbons.ExcelRibbon.ListTablePics.Items.Add(emptyTablePic);

                RibbonDropDownItem emptyTablePic2 = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                emptyTablePic2.Label = Constants.Blank;

                Globals.Ribbons.ExcelRibbon.ListInfoPoints.Items.Add(emptyTablePic2);

                foreach (Api.DAL.DataTable item in dataTableList)
                {
                    RibbonDropDownItem newTablePic = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                    newTablePic.Label = item.Name;
                    newTablePic.OfficeImageId = "CreateHandoutsInWord";
                    if (item.Type == DataType.InfoTable)
                    {
                        Globals.Ribbons.ExcelRibbon.ListTablePics.Items.Add(newTablePic);
                    }
                    if (item.Type == DataType.InfoPoint)
                    {
                        Globals.Ribbons.ExcelRibbon.ListInfoPoints.Items.Add(newTablePic);
                    }
                }
            }
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            UnhookWinEvent(this.windowEventHook);
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
