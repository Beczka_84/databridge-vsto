﻿using DataBridge.Api.DAL;
using DataBridge.Api.Model;
using DataBridge.Api.Service;
using Microsoft.Office.Tools.Ribbon;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataBridge.Word
{
    public partial class DissacateForm : Form
    {
        private static DissacateForm inst;
        private List<Api.DAL.Excel> _list { get; set; }


        public static void Create(List<Api.DAL.Excel> list)
        {
            if (inst == null || inst.IsDisposed)
                inst = new DissacateForm(list);

        }

        public DissacateForm(List<Api.DAL.Excel> list)
        {
            this._list = list;

            InitializeComponent();

            this.comboExelNames.DataSource = this._list;
            this.comboExelNames.DisplayMember = "Name";
            this.comboExelNames.ValueMember = "Id";

            this.Show();
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            if (this.comboExelNames.SelectedItem != null)
            {
                Api.DAL.Excel obj = (Api.DAL.Excel)this.comboExelNames.SelectedItem;

                ExcelVM excelVM = new ExcelVM();
                excelVM.Path = obj.Path;
                excelVM.Name = obj.Name;

                WordVM wordVM = new WordVM();
                wordVM.Path = Globals.ThisAddIn.Application.ActiveDocument.Path;
                wordVM.Name = Globals.ThisAddIn.Application.ActiveDocument.Name;

                WordService wordService = new WordService();
                ServiceResult serviceResult = wordService.DissociateExcel(wordVM, excelVM);

                if (serviceResult.Sucess)
                {
                    Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Clear();
                    Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.Clear();
                    Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.Clear();

                    RibbonDropDownItem empty = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                    empty.Label = Constants.Blank;

                    RibbonDropDownItem empty2 = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                    empty2.Label = Constants.Blank;

                    RibbonDropDownItem empty3 = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                    empty3.Label = Constants.Blank;

                    Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Add(empty);
                    Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.Add(empty2);
                    Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.Add(empty3);

                    List<Api.DAL.Excel> excelList = wordService.GetAssociatedExcels(wordVM).ToList();

                    foreach (Api.DAL.Excel item in excelList)
                    {
                        RibbonDropDownItem excel = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                        excel.Label = item.Name;
                        excel.OfficeImageId = "ExportExcel";
                        Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Add(excel);
                    }
                }
                else
                {
                    MessageBox.Show(string.Format("Error : {0}", serviceResult.ErrorMessage), "DataBridge.Word");
                }
                this.Close();
               

            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
