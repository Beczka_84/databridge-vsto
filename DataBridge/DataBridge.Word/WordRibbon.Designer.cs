﻿namespace DataBridge.Word
{
    partial class WordRibbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public WordRibbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WORD_DATA_BRIDGE = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.AssociateButton = this.Factory.CreateRibbonButton();
            this.DissociateButton = this.Factory.CreateRibbonButton();
            this.FinalBtn = this.Factory.CreateRibbonButton();
            this.HighlightBtn = this.Factory.CreateRibbonButton();
            this.separator1 = this.Factory.CreateRibbonSeparator();
            this.ListOfAssociatedExcel = this.Factory.CreateRibbonDropDown();
            this.separator2 = this.Factory.CreateRibbonSeparator();
            this.ListOfAvailableInfoPoints = this.Factory.CreateRibbonDropDown();
            this.ListOfAvailableTablePictures = this.Factory.CreateRibbonDropDown();
            this.separator3 = this.Factory.CreateRibbonSeparator();
            this.InsertIP = this.Factory.CreateRibbonButton();
            this.InsertTP = this.Factory.CreateRibbonButton();
            this.separator4 = this.Factory.CreateRibbonSeparator();
            this.Goto = this.Factory.CreateRibbonButton();
            this.Update = this.Factory.CreateRibbonButton();
            this.UpdateIP = this.Factory.CreateRibbonButton();
            this.WORD_DATA_BRIDGE.SuspendLayout();
            this.group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // WORD_DATA_BRIDGE
            // 
            this.WORD_DATA_BRIDGE.Groups.Add(this.group1);
            this.WORD_DATA_BRIDGE.Label = "DATA BRIDGE";
            this.WORD_DATA_BRIDGE.Name = "WORD_DATA_BRIDGE";
            // 
            // group1
            // 
            this.group1.Items.Add(this.AssociateButton);
            this.group1.Items.Add(this.DissociateButton);
            this.group1.Items.Add(this.FinalBtn);
            this.group1.Items.Add(this.HighlightBtn);
            this.group1.Items.Add(this.separator1);
            this.group1.Items.Add(this.ListOfAssociatedExcel);
            this.group1.Items.Add(this.separator2);
            this.group1.Items.Add(this.ListOfAvailableInfoPoints);
            this.group1.Items.Add(this.ListOfAvailableTablePictures);
            this.group1.Items.Add(this.separator3);
            this.group1.Items.Add(this.InsertIP);
            this.group1.Items.Add(this.InsertTP);
            this.group1.Items.Add(this.separator4);
            this.group1.Items.Add(this.Goto);
            this.group1.Items.Add(this.Update);
            this.group1.Items.Add(this.UpdateIP);
            this.group1.Name = "group1";
            // 
            // AssociateButton
            // 
            this.AssociateButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.AssociateButton.Label = "Associate";
            this.AssociateButton.Name = "AssociateButton";
            this.AssociateButton.OfficeImageId = "ExportWord";
            this.AssociateButton.ShowImage = true;
            this.AssociateButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AssociateButton_Click);
            // 
            // DissociateButton
            // 
            this.DissociateButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.DissociateButton.Label = "Dissociate";
            this.DissociateButton.Name = "DissociateButton";
            this.DissociateButton.OfficeImageId = "PrintPreviewClose";
            this.DissociateButton.ShowImage = true;
            this.DissociateButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.DissociateButton_Click);
            // 
            // FinalBtn
            // 
            this.FinalBtn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.FinalBtn.Label = "Final";
            this.FinalBtn.Name = "FinalBtn";
            this.FinalBtn.OfficeImageId = "FileMarkAsFinal";
            this.FinalBtn.ShowImage = true;
            this.FinalBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.FinalBtn_Click);
            // 
            // HighlightBtn
            // 
            this.HighlightBtn.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.HighlightBtn.Label = "Highlight Info";
            this.HighlightBtn.Name = "HighlightBtn";
            this.HighlightBtn.OfficeImageId = "FileDocumentManagementInformation";
            this.HighlightBtn.ShowImage = true;
            this.HighlightBtn.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.HighlightBtn_Click);
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            // 
            // ListOfAssociatedExcel
            // 
            this.ListOfAssociatedExcel.Label = "List of Associated Excel";
            this.ListOfAssociatedExcel.Name = "ListOfAssociatedExcel";
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            // 
            // ListOfAvailableInfoPoints
            // 
            this.ListOfAvailableInfoPoints.Label = "List of Available Info Points";
            this.ListOfAvailableInfoPoints.Name = "ListOfAvailableInfoPoints";
            // 
            // ListOfAvailableTablePictures
            // 
            this.ListOfAvailableTablePictures.Label = "List of Available Table Pictures";
            this.ListOfAvailableTablePictures.Name = "ListOfAvailableTablePictures";
            // 
            // separator3
            // 
            this.separator3.Name = "separator3";
            // 
            // InsertIP
            // 
            this.InsertIP.Label = "Insert ";
            this.InsertIP.Name = "InsertIP";
            this.InsertIP.OfficeImageId = "SlideMasterTextPlaceholderInsert";
            this.InsertIP.ShowImage = true;
            this.InsertIP.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.InsertIP_Click);
            // 
            // InsertTP
            // 
            this.InsertTP.Label = "Insert";
            this.InsertTP.Name = "InsertTP";
            this.InsertTP.OfficeImageId = "SlideMasterPicturePlaceholderInsert";
            this.InsertTP.ShowImage = true;
            this.InsertTP.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.InsertTP_Click);
            // 
            // separator4
            // 
            this.separator4.Name = "separator4";
            // 
            // Goto
            // 
            this.Goto.Label = "Goto Source ";
            this.Goto.Name = "Goto";
            this.Goto.OfficeImageId = "GoToNewRecord";
            this.Goto.ShowImage = true;
            this.Goto.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.Goto_Click);
            // 
            // Update
            // 
            this.Update.Label = "Update";
            this.Update.Name = "Update";
            this.Update.OfficeImageId = "QueryUpdate";
            this.Update.ShowImage = true;
            this.Update.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.Update_Click);
            // 
            // UpdateIP
            // 
            this.UpdateIP.Label = "";
            this.UpdateIP.Name = "UpdateIP";
            // 
            // WordRibbon
            // 
            this.Name = "WordRibbon";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.WORD_DATA_BRIDGE);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.WordRibbon_Load);
            this.WORD_DATA_BRIDGE.ResumeLayout(false);
            this.WORD_DATA_BRIDGE.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab WORD_DATA_BRIDGE;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AssociateButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton DissociateButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton FinalBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton HighlightBtn;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator1;
        internal Microsoft.Office.Tools.Ribbon.RibbonDropDown ListOfAssociatedExcel;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator2;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton InsertIP;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton UpdateIP;
        internal Microsoft.Office.Tools.Ribbon.RibbonDropDown ListOfAvailableInfoPoints;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator3;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton InsertTP;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton Update;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton Goto;
        internal Microsoft.Office.Tools.Ribbon.RibbonDropDown ListOfAvailableTablePictures;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator4;
    }

    partial class ThisRibbonCollection
    {
        internal WordRibbon WordRibbon
        {
            get { return this.GetRibbon<WordRibbon>(); }
        }
    }
}
