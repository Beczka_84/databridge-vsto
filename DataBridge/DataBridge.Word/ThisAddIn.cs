﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using DataBridge.Api.Service;
using DataBridge.Api.Model;
using DataBridge.Api.DAL;
using Microsoft.Office.Tools.Ribbon;
using DataBridge.Word.Services;

namespace DataBridge.Word
{
    public partial class ThisAddIn
    {

        private IntPtr windowEventHook;
        private delegate void WinEventProc(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime);
        private WinEventProc winEventProc;

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr SetWinEventHook(int eventMin, int eventMax, IntPtr hmodWinEventProc, WinEventProc lpfnWinEventProc, int idProcess, int idThread, int dwflags);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int UnhookWinEvent(IntPtr hWinEventHook);

        private const int WINEVENT_INCONTEXT = 4;
        private const int WINEVENT_OUTOFCONTEXT = 0;
        private const int WINEVENT_SKIPOWNPROCESS = 2;
        private const int WINEVENT_SKIPOWNTHREAD = 1;

        private const int EVENT_SYSTEM_FOREGROUND = 3;

        public void SubscribeToWindowEvents()
        {
            this.winEventProc = WindowEventCallback;
            if (windowEventHook == IntPtr.Zero)
            {
                windowEventHook = SetWinEventHook(
                    EVENT_SYSTEM_FOREGROUND, // eventMin
                    EVENT_SYSTEM_FOREGROUND, // eventMax
                    IntPtr.Zero, // hmodWinEventProc
                    this.winEventProc, // lpfnWinEventProc
                    0, // idProcess
                    0, // idThread
                    WINEVENT_OUTOFCONTEXT);
            }
        }

        void WindowEventCallback(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime)
        {
            if(Globals.ThisAddIn.Application.ActiveWindow != null)
            {
                if (eventType == 3)
                {
                    if (hwnd == (IntPtr) Globals.ThisAddIn.Application.ActiveWindow.Hwnd)
                    {
                        SetUpRibbon();
                        //MessageBox.Show(hwnd.ToString());
                    }
                }
            }
        }


        private Dictionary<string, SelectedWordItemsDropdowns> _SelectedItems;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            _SelectedItems = new Dictionary<string, SelectedWordItemsDropdowns>();
            SetUpRibbon();
            this.Application.WindowActivate += Application_WindowActivate;
            this.Application.DocumentOpen += Application_DocumentOpen;
            this.Application.Startup += Application_Startup;
            this.Application.WindowDeactivate += Application_WindowDeactivate;
        }

        private void Application_WindowDeactivate(Microsoft.Office.Interop.Word.Document Doc, Microsoft.Office.Interop.Word.Window Wn)
        {
            SelectedWordItemsDropdowns selected = new SelectedWordItemsDropdowns();

            selected.Excel = Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.SelectedItem;
            selected.Ip = Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.SelectedItem;
            selected.Tp = Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.SelectedItem;

            _SelectedItems[Doc.Name] = selected;
        }


        private void Application_Startup()
        {
            SetUpRibbon();
        }

        private void Application_DocumentOpen(Microsoft.Office.Interop.Word.Document Doc)
        {
            SetUpRibbon();
        }

        private void Application_WindowActivate(Microsoft.Office.Interop.Word.Document Doc, Microsoft.Office.Interop.Word.Window Wn)
        {
            SetUpRibbon();
        }

        private void SetUpRibbon()
        {

            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count > 0)
                {
                    Globals.Ribbons.WordRibbon.DissociateButton.Enabled = false;

                    WordVM wordVM = new WordVM();
                    wordVM.Path = Globals.ThisAddIn.Application.ActiveDocument.Path;
                    wordVM.Name = Globals.ThisAddIn.Application.ActiveDocument.Name;

                    WordService wordService = new WordService();
                    int associatedExcelCount = wordService.GetAssociatedExcelsCount(wordVM);

                    if (associatedExcelCount > 0)
                    {
                        Globals.Ribbons.WordRibbon.DissociateButton.Enabled = true;

                        Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Clear();
                        Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.Clear();
                        Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.Clear();

                        RibbonDropDownItem empty = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                        empty.Label = Constants.Blank;

                        RibbonDropDownItem empty2 = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                        empty2.Label = Constants.Blank;

                        RibbonDropDownItem empty3 = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                        empty3.Label = Constants.Blank;

                        Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Add(empty);
                        Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.Add(empty2);
                        Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.Add(empty3);

                        List<Api.DAL.Excel> excelList = wordService.GetAssociatedExcels(wordVM).ToList();

                        foreach (Api.DAL.Excel item in excelList)
                        {
                            RibbonDropDownItem excel = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                            excel.Label = item.Name;
                            excel.OfficeImageId = "ExportExcel";
                            Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Add(excel);

                        }

                        if (associatedExcelCount == 1)
                        {
                            Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Clear();
                            var excelItem = excelList.First();
                            RibbonDropDownItem excel = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                            excel.Label = excelItem.Name;
                            excel.OfficeImageId = "ExportExcel";
                            Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Add(excel);

                            foreach (DataTable item in excelList.First().DataTables)
                            {
                                if (item.Type == DataType.InfoPoint)
                                {
                                    RibbonDropDownItem infoPoint =
                                        Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                                    infoPoint.Label = item.Name;
                                    infoPoint.OfficeImageId = "CreateHandoutsInWord";
                                    Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.Add(infoPoint);
                                }

                                if (item.Type == DataType.InfoTable)
                                {
                                    RibbonDropDownItem tablePic =
                                        Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                                    tablePic.Label = item.Name;
                                    tablePic.OfficeImageId = "SignatureLineInsert";
                                    Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.Add(tablePic);
                                }

                            }
                        }

                        if (associatedExcelCount > 1)
                        {
                            if (_SelectedItems.ContainsKey(Globals.ThisAddIn.Application.ActiveDocument.Name))
                            {
                                foreach (
                                    DataTable item in
                                        excelList.Where(
                                            x =>
                                                x.Name ==
                                                _SelectedItems[Globals.ThisAddIn.Application.ActiveDocument.Name].Excel
                                                    .Label).First().DataTables)
                                {
                                    if (item.Type == DataType.InfoPoint)
                                    {
                                        RibbonDropDownItem infoPoint =
                                            Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                                        infoPoint.Label = item.Name;
                                        infoPoint.OfficeImageId = "CreateHandoutsInWord";
                                        Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.Add(infoPoint);
                                    }

                                    if (item.Type == DataType.InfoTable)
                                    {
                                        RibbonDropDownItem tablePic =
                                            Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                                        tablePic.Label = item.Name;
                                        tablePic.OfficeImageId = "SignatureLineInsert";
                                        Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.Add(tablePic);
                                    }

                                }
                            }
                        }



                        if (_SelectedItems.ContainsKey(Globals.ThisAddIn.Application.ActiveDocument.Name))
                        {
                            Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.SelectedItem =
                                Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.FirstOrDefault(
                                    x =>
                                        x.Label ==
                                        _SelectedItems[Globals.ThisAddIn.Application.ActiveDocument.Name].Excel.Label);
                            Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.SelectedItem =
                                Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.FirstOrDefault(
                                    x =>
                                        x.Label ==
                                        _SelectedItems[Globals.ThisAddIn.Application.ActiveDocument.Name].Ip.Label);
                            Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.SelectedItem =
                                Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.FirstOrDefault(
                                    x =>
                                        x.Label ==
                                        _SelectedItems[Globals.ThisAddIn.Application.ActiveDocument.Name].Tp.Label);
                        }

                    }
                    else
                    {
                        Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Clear();
                        Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.Clear();
                        Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.Clear();
                    }
                }
            }
            catch (Exception)
            {
            }

        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            UnhookWinEvent(this.windowEventHook);
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        #endregion
    }
}
