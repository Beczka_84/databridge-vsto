﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Core;
using WordApp = Microsoft.Office.Interop.Word;

namespace DataBridge.Word
{

    public partial class CreateGraphicsForm : Form
    {
        private Microsoft.Office.Interop.Word.ContentControl pictureControl { get; set; }
        private string PathToNewFile { get; set; }

        public CreateGraphicsForm()
        {
            InitializeComponent();
            this.txtSelected.Enabled = false;
        
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                this.txtSelected.Enabled = true;
                this.txtSelected.Text = openFileDialog1.FileName;
                PathToNewFile = openFileDialog1.FileName;
            }
        }

        private void createbtn_Click(object sender, EventArgs e)
        {
            if (this.txtNameCC.Text == string.Empty)
            {
                MessageBox.Show(string.Format("No name for content control", "DataBridge.Excel"));
                return;
            }

            if (PathToNewFile != null)
            {
                pictureControl = Globals.ThisAddIn.Application.ActiveDocument.ContentControls.Add(Microsoft.Office.Interop.Word.WdContentControlType.wdContentControlPicture);
                pictureControl.Range.InlineShapes[1].Delete();
                pictureControl.Range.InlineShapes.AddPicture(openFileDialog1.FileName);
                pictureControl.Range.InlineShapes[1].Width = 200;
                pictureControl.Range.InlineShapes[1].Height = 300;
                pictureControl.Title = this.txtNameCC.Text;
                CreateCustomXmlPart(this.txtNameCC.Text);
                this.Close();
            }
        }

        private void cancelbtn_Click(object sender, EventArgs e)
        {
            if (this.txtNameCC.Text == string.Empty)
            {
                MessageBox.Show(string.Format("No name for content control", "DataBridge.Excel"));
                return;
            }

            pictureControl = Globals.ThisAddIn.Application.ActiveDocument.ContentControls.Add(Microsoft.Office.Interop.Word.WdContentControlType.wdContentControlPicture);
            Image temImage = imageList1.Images[0];
            string path = string.Join(@"\", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "placeholder.png");
            temImage.Save(path, System.Drawing.Imaging.ImageFormat.Png);
            pictureControl.Range.InlineShapes.AddPicture(path);
            System.IO.File.Delete(path);
            pictureControl.Title = this.txtNameCC.Text;
            CreateCustomXmlPart(this.txtNameCC.Text);
            this.Close();
        }

        private void CreateCustomXmlPart(string name)
        {
            string content = "";
            content += "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Graphics>";
            content = FindCustomXmlPart().Aggregate(content, (current, item) => current + ("<name>" + item + "</name>"));
            content += "<name>" + name + "</name>";
            content += "</Graphics>";

            var part = Globals.ThisAddIn.Application.ActiveDocument.CustomXMLParts.Add(content, System.Type.Missing);
        }

        private static void GetGraphics(CustomXMLPart part)
        {
            StringReader reader = new StringReader(part.XML);
            XElement xelement = XElement.Load(reader);
            IEnumerable<XElement> infoPoints = xelement.Elements();

            foreach (var infoPoint in infoPoints)
            {
                //todo
            }
        }

        private List<string> FindCustomXmlPart()
        {
            string xmlString = "";
            List<string> list = new List<string>();
            foreach (CustomXMLPart XMLPart in Globals.ThisAddIn.Application.ActiveDocument.CustomXMLParts)
            {
                if ("Graphics" == XMLPart.SelectSingleNode("/*").BaseName)
                {
                    xmlString = XMLPart.XML;
                    XMLPart.Delete();
                }
            }

            if (xmlString != string.Empty)
            {
                StringReader reader = new StringReader(xmlString);
                XElement xelement = XElement.Load(reader);
                IEnumerable<XElement> infoPoints = xelement.Elements();
                list = infoPoints.Select(infoPoint => infoPoint.Value).ToList();
            }
            return list;
        }
    }
}
