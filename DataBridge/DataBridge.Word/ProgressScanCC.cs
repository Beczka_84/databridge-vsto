﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataBridge.Api.DAL;
using DataBridge.Api.Model;
using DataBridge.Api.Service;
using DataBridge.Word;
using DataBridge.Word.Services;
using WordApp = Microsoft.Office.Interop.Word;
using Microsoft.Office.Core;
using ExcelApp = Microsoft.Office.Interop.Excel;


namespace DataBridge.Excel
{

    public partial class ProgressScanCCWord : Form
    {

        //[DllImport("user32.dll", CharSet = CharSet.Auto)]
        //private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

        //[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        //public static extern int GetWindowThreadProcessId(HandleRef handle, out int processId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowThreadProcessId(HandleRef handle, out int processId);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

        private ExcelVM _excelVM { get; set; }
        private WordService _wordService { get; set; }
        private WordVM _wordVM { get; set; }


        public ProgressScanCCWord(ExcelVM excelVM, WordService wordService, WordVM wordVM)
        {
            InitializeComponent();

            this._excelVM = excelVM;
            this._wordService = wordService;
            this._wordVM = wordVM;

            this.Show();
        }

        private void ProgressScanCC_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            string[] progIds = { "Excel.Application" };
            ExcelApp.Application app = null;
            ExcelApp.Workbooks wrbs = null;
            ExcelApp.Workbook wrb = null;
            int hWnd = 0;
            int pid = -1;
            bool AttachToexisting = false;
            try
            {
                ExcelInstances excelInstaces = new ExcelInstances();
                List<object> instances = excelInstaces.GetRunningInstances(progIds);


                foreach (ExcelApp.Application excelApp in instances)
                {
                    app = excelApp;
                    wrbs = app.Workbooks;
                    foreach (ExcelApp.Workbook singleWrb in wrbs)
                    {
                        if (singleWrb.Name == _excelVM.Name)
                        {
                            wrb = singleWrb;
                            wrb.Save();
                            this.label1.Text = "Attaching to open excel document ....";
                            AttachToexisting = true;
                            break;
                        }
                    }
                }

            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                app = new ExcelApp.Application();
                wrbs = app.Workbooks;
                wrb = wrbs.Open(_excelVM.FullPath);
                this.label1.Text = "Open excel document ....";
            }

            if (wrb == null)
            {
                app = new ExcelApp.Application();
                wrbs = app.Workbooks;
                wrb = wrbs.Open(_excelVM.FullPath);
                this.label1.Text = "Open excel document ....";

            }
            List<string> namesList = new List<string>();
            this.label1.Text = "Getting names from excel document ....";

            foreach (ExcelApp.Name excelName in wrb.Names)
            {
                if (excelName.Name.Contains(Api.Model.Constants.IPPrefix) || excelName.Name.Contains(Api.Model.Constants.TPPrefix))
                {
                    namesList.Add(excelName.Name);
                }
            }

            if (!AttachToexisting)
            {
                Process[] excelProcs = Process.GetProcessesByName("EXCEL");
                HandleRef hwnd = new HandleRef(app, (IntPtr)app.Hwnd);
                GetWindowThreadProcessId(hwnd, out pid);
                foreach (Process proc in excelProcs)
                {
                    if (proc.Id == pid)
                    {
                        proc.Kill();
                    }
                }
                SendMessage((IntPtr)hWnd, 0x10, IntPtr.Zero, IntPtr.Zero);
            }

            //wrb.Close();
            //app.Application.Quit();
            //app.Quit();


            //Marshal.ReleaseComObject(wrb);
            //Marshal.ReleaseComObject(wrbs);
            //Marshal.ReleaseComObject(app);
            //GC.Collect();
            //GC.WaitForPendingFinalizers();

            //if (!AttachToexisting)
            //{
            //    int hWnd = 0;
            //    int pid = -1;
            //    //wrb.Close();
            //    //app.Quit();
            //    Process[] excelProcs = Process.GetProcessesByName("EXCEL");
            //    HandleRef hwnd = new HandleRef(app, (IntPtr)app.Hwnd);
            //    GetWindowThreadProcessId(hwnd, out pid);
            //    foreach (Process proc in excelProcs)
            //    {
            //        if (proc.Id == pid)
            //        {
            //            proc.Kill();
            //        }
            //    }
            //    SendMessage((IntPtr)hWnd, 0x10, IntPtr.Zero, IntPtr.Zero);
            //}


            try
            {

                Api.DAL.Excel excel = _wordService.GetAssociatedExcels(_wordVM).FirstOrDefault(x => x.FullPath == _excelVM.FullPath);

                if (excel == null)
                {
                    this.Close();
                }

                this.label1.Text = "Starting ....";
                this.progressBar1.Maximum = Globals.ThisAddIn.Application.ActiveDocument.ContentControls.Count;

                for (int i = 0; i < Globals.ThisAddIn.Application.ActiveDocument.ContentControls.Count; i++)
                {
                    this.label1.Text = string.Format("Scaning word content controls {0} of {1}", i + 1, Globals.ThisAddIn.Application.ActiveDocument.ContentControls.Count);
                    this.progressBar1.Value = i + 1;

                    Microsoft.Office.Interop.Word.ContentControl item = Globals.ThisAddIn.Application.ActiveDocument.ContentControls[i + 1];

                    if (namesList.Contains(item.Title))
                    {
                        if (item.Title.Contains(Constants.IPPrefix))
                        {
                            item.Tag = excel.Id.ToString();
                        }

                        if (item.Title.Contains(Constants.TPPrefix))
                        {
                            item.Tag = excel.Id.ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error : {0}", ex.Message), "DataBridge.Word");
                //wrb.Close();
                //app.Quit();
                this.Close();
            }
            //app.ScreenUpdating = true;
            //app.Visible = true;
            this.Close();
        }
    }
}
