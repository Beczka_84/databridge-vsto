﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using DataBridge.Api.Model;
using DataBridge.Api.Service;
using System.Windows.Forms;
using DataBridge.Api.DAL;
using Microsoft.Office.Tools.Word;
using DataBridge.Word.Services;
using System.Drawing;
using System.IO;
using ExcelApp = Microsoft.Office.Interop.Excel;
using System.Collections;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Reflection;
using System.Runtime.InteropServices;
using DataBridge.Excel;
using ImageMagick;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using ContentControl = Microsoft.Office.Interop.Word.ContentControl;
using DataTable = DataBridge.Api.DAL.DataTable;
using Task = System.Threading.Tasks.Task;

namespace DataBridge.Word
{
    public partial class WordRibbon
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowThreadProcessId(HandleRef handle, out int processId);

        private Dictionary<string, bool> _highlightDataPoints;
        private void WordRibbon_Load(object sender, RibbonUIEventArgs e)
        {
            this.ListOfAssociatedExcel.SelectionChanged += ListOfAssociatedExcel_SelectionChanged;
            _highlightDataPoints = new Dictionary<string, bool>();
        }

        private void ListOfAssociatedExcel_SelectionChanged(object sender, RibbonControlEventArgs e)
        {
            WordVM wordVM = new WordVM();
            wordVM.Path = Globals.ThisAddIn.Application.ActiveDocument.Path;
            wordVM.Name = Globals.ThisAddIn.Application.ActiveDocument.Name;

            WordService wordService = new WordService();
            Api.DAL.Excel excel = wordService.GetAssociatedExcels(wordVM).FirstOrDefault(x => x.Name == this.ListOfAssociatedExcel.SelectedItem.Label);

            ExcelVM excelVM = new ExcelVM();
            excelVM.Path = excel.Path;
            excelVM.Name = excel.Name;

            IEnumerable<DataTable> dataTables = wordService.GetDataTables(wordVM, excelVM);

            Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.Clear();
            Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.Clear();

            RibbonDropDownItem empty = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
            empty.Label = Constants.Blank;

            RibbonDropDownItem empty2 = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
            empty2.Label = Constants.Blank;

            Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.Add(empty);
            Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.Add(empty2);


            foreach (DataTable item in dataTables)
            {
                if (item.Type == DataType.InfoPoint)
                {
                    RibbonDropDownItem infoPoint = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                    infoPoint.Label = item.Name;
                    infoPoint.OfficeImageId = "CreateHandoutsInWord";
                    Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.Add(infoPoint);
                }

                if (item.Type == DataType.InfoTable)
                {
                    RibbonDropDownItem tablePic = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                    tablePic.Label = item.Name;
                    tablePic.OfficeImageId = "SignatureLineInsert";
                    Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.Add(tablePic);
                }

            }

        }

        private void AssociateButton_Click(object sender, RibbonControlEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Filter = "Excel Files | *.xlsx; *.xlsm; *.xls";
            DialogResult result = dialog.ShowDialog();


            if (result == DialogResult.Cancel)
            {
                MessageBox.Show("You canceled the file open dialog");
            }

            if (result == DialogResult.OK)
            {
                ExcelVM excelVM = new ExcelVM();
                excelVM.Path = System.IO.Path.GetDirectoryName(dialog.FileName);
                excelVM.Name = System.IO.Path.GetFileName(dialog.FileName);

                WordVM wordVM = new WordVM();
                wordVM.Path = Globals.ThisAddIn.Application.ActiveDocument.Path;
                wordVM.Name = Globals.ThisAddIn.Application.ActiveDocument.Name;

                WordService wordService = new WordService();
                ServiceResult serviceResult = wordService.AssociateExcel(wordVM, excelVM);

                if (serviceResult.Sucess)
                {
                    Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Clear();
                    Globals.Ribbons.WordRibbon.DissociateButton.Enabled = true;

                    RibbonDropDownItem empty = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                    empty.Label = Constants.Blank;

                    List<Api.DAL.Excel> excelList = wordService.GetAssociatedExcels(wordVM).ToList();

                    ProgressScanCCWord progres = new ProgressScanCCWord(excelVM, wordService, wordVM);

                    foreach (Api.DAL.Excel item in excelList)
                    {
                        RibbonDropDownItem excel = Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
                        excel.Label = item.Name;
                        excel.OfficeImageId = "ExportExcel";
                        Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Add(excel);
                    }
                }
                else
                {
                    MessageBox.Show(string.Format("Error : {0}", serviceResult.ErrorMessage), "DataBridge.Word");
                }
            }
        }

        private void DissociateButton_Click(object sender, RibbonControlEventArgs e)
        {
            WordVM wordVM = new WordVM();
            wordVM.Path = Globals.ThisAddIn.Application.ActiveDocument.Path;
            wordVM.Name = Globals.ThisAddIn.Application.ActiveDocument.Name;

            WordService wordService = new WordService();
            List<Api.DAL.Excel> excelList = wordService.GetAssociatedExcels(wordVM).ToList();
            DissacateForm.Create(excelList);
        }

        private void InsertIP_Click(object sender, RibbonControlEventArgs e)
        {
            if (this.ListOfAvailableInfoPoints.SelectedItem.Label != Constants.Blank)
            {
                ServiceResult result = null;

                WordVM wordVM = new WordVM();
                wordVM.Path = Globals.ThisAddIn.Application.ActiveDocument.Path;
                wordVM.Name = Globals.ThisAddIn.Application.ActiveDocument.Name;

                WordService wordService = new WordService();

                Api.DAL.Excel excel = wordService.GetAssociatedExcels(wordVM).FirstOrDefault(x => x.Name == this.ListOfAssociatedExcel.SelectedItem.Label);

                ExcelVM excelVM = new ExcelVM();
                excelVM.Path = excel.Path;
                excelVM.Name = excel.Name;

                IEnumerable<DataTable> dataTables = wordService.GetDataTables(wordVM, excelVM);
                DataTable dataTable = dataTables.FirstOrDefault(x => x.Name == this.ListOfAvailableInfoPoints.SelectedItem.Label && x.Type == DataType.InfoPoint);

                if (dataTable != null)
                {
                    result = OpenAndRefreshExcel(excelVM, dataTable.Name, DataType.InfoPoint);
                }

                try
                {
                    if (result != null && result.Sucess)
                    {
                        ContentControl txtControl =
                            Globals.ThisAddIn.Application.ActiveDocument.ContentControls.Add(
                                WdContentControlType.wdContentControlRichText);
                        if (string.IsNullOrEmpty(dataTable.Value))
                        {
                            txtControl.Range.Text = dataTable.Name;
                            txtControl.Range.HighlightColorIndex = WdColorIndex.wdYellow;
                        }
                        else
                        {
                            txtControl.Range.Text = dataTable.Value;
                            txtControl.Range.HighlightColorIndex = WdColorIndex.wdNoHighlight;
                        }
                        txtControl.Title = dataTable.Name;
                        txtControl.Tag = dataTable.ExcelFileId.ToString(); //excelVM.FullPath;}
                    }

                }
                catch (Microsoft.Office.Tools.ControlNameAlreadyExistsException ex)
                {
                    MessageBox.Show(string.Format("Error : {0}", ex.Message), "DataBridge.Word");
                }

            }
        }

        private void InsertTP_Click(object sender, RibbonControlEventArgs e)
        {
            if (this.ListOfAvailableTablePictures.SelectedItem.Label != Constants.Blank)
            {
                ServiceResult result = null;

                WordVM wordVM = new WordVM();
                wordVM.Path = Globals.ThisAddIn.Application.ActiveDocument.Path;
                wordVM.Name = Globals.ThisAddIn.Application.ActiveDocument.Name;

                WordService wordService = new WordService();

                Api.DAL.Excel excel = wordService.GetAssociatedExcels(wordVM).FirstOrDefault(x => x.Name == this.ListOfAssociatedExcel.SelectedItem.Label);

                ExcelVM excelVM = new ExcelVM();
                excelVM.Path = excel.Path;
                excelVM.Name = excel.Name;

                IEnumerable<DataTable> dataTables = wordService.GetDataTables(wordVM, excelVM);
                DataTable dataTable = dataTables.FirstOrDefault(x => x.Name == this.ListOfAvailableTablePictures.SelectedItem.Label && x.Type == DataType.InfoTable);

                if (dataTable != null)
                {
                    result = OpenAndRefreshExcel(excelVM, dataTable.Name, DataType.InfoTable);
                }

                try
                {
                    //Old way
                    //Microsoft.Office.Interop.Word.ContentControl pictureControl = Globals.ThisAddIn.Application.ActiveDocument.ContentControls.Add(Microsoft.Office.Interop.Word.WdContentControlType.wdContentControlPicture);
                    if (result != null && result.Sucess)
                    {
                        ContentControl pictureControl = Globals.ThisAddIn.Application.ActiveDocument.ContentControls.Add(WdContentControlType.wdContentControlRichText);
                        ByteToImageService byteToImageService = new ByteToImageService();

                        MagickImage image = byteToImageService.GetImageFromBytes(dataTable.Image);

                        if (image != null)
                        {
                            var pagesetup = Globals.ThisAddIn.Application.ActiveDocument.PageSetup;
                            MagickImage image2 = byteToImageService.ResizeImage(image, pagesetup);
                            string path = string.Join(@"\",Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "temp.png");
                            image2.Write(path);
                            //Old way
                            pictureControl.SetPlaceholderText(null, null, string.Empty);
                            var ilineshape = pictureControl.Range.InlineShapes.AddPicture(path);
                            var shape = ilineshape.ConvertToShape();
                            shape.WrapFormat.Type = WdWrapType.wdWrapInline;
                            shape.LockAspectRatio = MsoTriState.msoCTrue;
                            pictureControl.Title = dataTable.Name;
                            pictureControl.Tag = dataTable.ExcelFileId.ToString(); //excelVM.FullPath;
                            File.Delete(path);
                        }
                    }
                }
                catch (Microsoft.Office.Tools.ControlNameAlreadyExistsException ex)
                {
                    MessageBox.Show(string.Format("Error : {0}", ex.Message), "DataBridge.Word");
                }

            }
        }

        private void Goto_Click(object sender, RibbonControlEventArgs e)
        {

            Microsoft.Office.Interop.Word.Selection selection = Globals.ThisAddIn.Application.Selection;
            Microsoft.Office.Interop.Word.Range rng = selection.Range;
            WordVM wordVM = new WordVM();
            wordVM.Path = Globals.ThisAddIn.Application.ActiveDocument.Path;
            wordVM.Name = Globals.ThisAddIn.Application.ActiveDocument.Name;

            WordService wordService = new WordService();


            foreach (Microsoft.Office.Interop.Word.ContentControl item in rng.ContentControls)
            {

                Api.DAL.Excel excel = wordService.GetAssociatedExcels(wordVM).FirstOrDefault(x => x.Id == Convert.ToInt16(item.Tag));
                if (excel == null)
                {
                    MessageBox.Show(string.Format("File : {0} don't exist", item.Title), "DataBridge.Word");
                }
                ExcelVM excelVM = new ExcelVM();
                excelVM.Path = excel.Path;
                excelVM.Name = excel.Name;

                IEnumerable<DataTable> dataTables = wordService.GetDataTables(wordVM, excelVM);

                if (item.Type == Microsoft.Office.Interop.Word.WdContentControlType.wdContentControlPicture)
                {
                    DataTable dataTable =
                        dataTables.FirstOrDefault(x => x.Name == item.Title && x.Type == DataType.InfoPoint);
                    Task.Run(() => OpenExcel(item, excelVM));
                }

                if (item.Type == Microsoft.Office.Interop.Word.WdContentControlType.wdContentControlRichText)
                {
                    DataTable dataTable =
                        dataTables.FirstOrDefault(x => x.Name == item.Title && x.Type == DataType.InfoPoint);
                    Task.Run(() => OpenExcel(item, excelVM));
                }
            }
        }

        private void OpenExcel(Microsoft.Office.Interop.Word.ContentControl item, ExcelVM excelVM)
        {

            string[] progIds = { "Excel.Application" };
            ExcelApp.Application app = null;
            ExcelApp.Workbook wrb = null;
            try
            {
                ExcelInstances excelInstaces = new ExcelInstances();
                List<object> instances = excelInstaces.GetRunningInstances(progIds);


                foreach (ExcelApp.Application excelApp in instances)
                {
                    //app = (ExcelApp.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
                    app = excelApp;
                    foreach (ExcelApp.Workbook singleWrb in app.Workbooks)
                    {
                        if (singleWrb.Name == Path.GetFileName(excelVM.FullPath))
                        {
                            wrb = singleWrb;
                            break;
                        }
                    }
                }

            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                app = new ExcelApp.Application();
                wrb = app.Workbooks.Open(excelVM.FullPath);
            }

            if (wrb == null)
            {
                app = new ExcelApp.Application();
                wrb = app.Workbooks.Open(excelVM.FullPath);
            }
            app.Visible = true;
            app.Windows[Path.GetFileName(excelVM.FullPath)].Activate();
            ExcelInstances excelActivates = new ExcelInstances();
            excelActivates.Activate((IntPtr)app.Hwnd);
            wrb.Activate();
            foreach (ExcelApp.Name excelName in wrb.Names)
            {
                if (excelName.Name == item.Title)
                {
                    ExcelApp.Range excelRng = excelName.RefersToRange;
                    ExcelApp.Worksheet sht = (ExcelApp.Worksheet)wrb.Worksheets[excelRng.Worksheet.Name.ToString()];
                    sht.Select();
                    excelRng.Select();

                    return;
                }
            }
            wrb.Close();
            app.Quit();
        }

        private ServiceResult OpenAndRefreshExcel(ExcelVM model, string dataPointName, DataType dataType)
        {
            Globals.ThisAddIn.Application.StatusBar = "Refreshing data ...";

            string[] progIds = { "Excel.Application" };
            ExcelApp.Application app = null;
            ExcelApp.Workbook wrb = null;
            int hWnd = 0;
            int pid = -1;
            bool AttachToexisting = false;
            try
            {
                ExcelInstances excelInstaces = new ExcelInstances();
                List<object> instances = excelInstaces.GetRunningInstances(progIds);


                foreach (ExcelApp.Application excelApp in instances)
                {
                    //app = (ExcelApp.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
                    app = excelApp;
                    foreach (ExcelApp.Workbook singleWrb in app.Workbooks)
                    {
                        if (singleWrb.Name == model.Name)
                        {
                            wrb = singleWrb;
                            wrb.Save();
                            AttachToexisting = true;
                            Globals.ThisAddIn.Application.StatusBar = "Attaching To existing excel app ...";
                            break;
                        }
                    }
                }

            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                app = new ExcelApp.Application();
                wrb = app.Workbooks.Open(model.FullPath);
                hWnd = app.Hwnd;
                Globals.ThisAddIn.Application.StatusBar = "Opening excel app ...";
            }

            if (wrb == null)
            {
                app = new ExcelApp.Application();
                wrb = app.Workbooks.Open(model.FullPath);
                hWnd = app.Hwnd;
                Globals.ThisAddIn.Application.StatusBar = "Opening excel app ...";
            }
            //app.Visible = true;
            foreach (ExcelApp.Name excelName in wrb.Names)
            {
                if (excelName.Name == dataPointName)
                {
                    try
                    {
                        DataTableVM dataPoint = new DataTableVM();
                        ByteToImageService byteToImageService = new ByteToImageService();
                        ExcelService excelService = new ExcelService();

                        if (dataType == DataType.InfoPoint)
                        {
                            dataPoint.Name = excelName.Name;
                            dataPoint.Type = DataType.InfoPoint;
                            ExcelApp.Range rng = excelName.RefersToRange;
                            ExcelApp.Range rngValue = rng.Cells[1, 1];
                            dataPoint.Value = rngValue.Text;
                        }

                        if (dataType == DataType.InfoTable)
                        {
                            dataPoint.Name = excelName.Name;
                            dataPoint.Type = DataType.InfoTable;
                            ExcelApp.Range rng = excelName.RefersToRange;
                            app.Visible = true;
                            //this.SuspendLayout();
                            ExcelApp.Worksheet sht = (ExcelApp.Worksheet)wrb.Worksheets[rng.Worksheet.Name.ToString()];
                            sht.Select();
                            rng.Select();
                            byte[] imageByteArray = byteToImageService.GetImageBytes(rng);
                            //app.Visible = false;
                            //this.ResumeLayout();
                            if (imageByteArray.Any())
                            {
                                dataPoint.Image = imageByteArray;
                            }
                        }
                      

                        ServiceResult serviceResult = excelService.UpdateDataTable(model, dataPoint);

                        if (!serviceResult.Sucess)
                        {
                            Globals.ThisAddIn.Application.StatusBar = "";
                            if (!AttachToexisting)
                            {
                                wrb.Close();
                                app.Quit();
                            }
                            return new ServiceResult() { Sucess = false, ErrorMessage = serviceResult.ErrorMessage };
                        }
                    }
                    catch (Exception e)
                    {
                        log.Error(e);
                    }
                    Globals.ThisAddIn.Application.StatusBar = "Done ...";
                    if (!AttachToexisting)
                    {
                        //wrb.Close();
                        //app.Quit();
                        Process[] excelProcs = Process.GetProcessesByName("EXCEL");
                        HandleRef hwnd = new HandleRef(app, (IntPtr)app.Hwnd);
                        GetWindowThreadProcessId(hwnd, out pid);
                        foreach (Process proc in excelProcs)
                        {
                            if (proc.Id == pid)
                            {
                                proc.Kill();
                            }
                        }
                        SendMessage((IntPtr)hWnd, 0x10, IntPtr.Zero, IntPtr.Zero);
                    }

                    return new ServiceResult() { Sucess = true, ErrorMessage = "" };
                }
            }
            Globals.ThisAddIn.Application.StatusBar = "Done ...";
            if (!AttachToexisting)
            {
                wrb.Close();
                app.Quit();
            }
            return new ServiceResult() { Sucess = true, ErrorMessage = "" };

        }

        private void Update_Click(object sender, RibbonControlEventArgs e)
        {
            Microsoft.Office.Interop.Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;

            Microsoft.Office.Interop.Word.Selection selection = Globals.ThisAddIn.Application.Selection;
            Microsoft.Office.Interop.Word.Range rng = selection.Range;

            WordVM wordVM = new WordVM();
            wordVM.Path = Globals.ThisAddIn.Application.ActiveDocument.Path;
            wordVM.Name = Globals.ThisAddIn.Application.ActiveDocument.Name;

            WordService wordService = new WordService();

            foreach (Microsoft.Office.Interop.Word.ContentControl item in rng.ContentControls)
            {
                Api.DAL.Excel excel = wordService.GetAssociatedExcels(wordVM).FirstOrDefault(x => x.Id == Convert.ToInt16(item.Tag));
                if (excel != null)
                {

                    ExcelVM excelVM = new ExcelVM();
                    excelVM.Path = excel.Path;
                    excelVM.Name = excel.Name;
                    if (item.Title.Contains(Constants.TPPrefix))
                    {
                        ServiceResult result = OpenAndRefreshExcel(excelVM, item.Title, DataType.InfoTable);
                        if (result.Sucess)
                        {
                            IEnumerable<DataTable> dataTables = wordService.GetDataTables(wordVM, excelVM);
                            DataTable dataTable = dataTables.FirstOrDefault(x => x.Name == item.Title && x.Type == DataType.InfoTable);

                            ByteToImageService byteToImageService = new ByteToImageService();
                            MagickImage image = byteToImageService.GetImageFromBytes(dataTable.Image);
                            if (image != null)
                            {
                                var pagesetup = Globals.ThisAddIn.Application.ActiveDocument.PageSetup;
                                MagickImage image2 = byteToImageService.ResizeImage(image, pagesetup);

                                try
                                {
                                    WdWrapType wrapp = WdWrapType.wdWrapSquare;
                                    float Height = 0;
                                    float Width = 0;
                                    string path = string.Join(@"\", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "temp.png");
                                    image2.Write(path);


                                    if (item.Range.ShapeRange.Count > 0)
                                    {
                                        wrapp = item.Range.ShapeRange[1].WrapFormat.Type;
                                        //Height = image.Height; // item.Range.ShapeRange[1].Height;
                                        Width = item.Range.ShapeRange[1].Width;

                                        item.Range.ShapeRange[1].Delete();
                                    }


                                    if (item.Range.InlineShapes.Count > 0)
                                    {
                                        //Height = image.Height; //item.Range.InlineShapes[1].Height;
                                        Width = item.Range.InlineShapes[1].Width;
                                        item.Range.InlineShapes[1].Delete();
                                    }

                                    item.SetPlaceholderText(null, null, string.Empty);
                                    var ilineshape = item.Range.InlineShapes.AddPicture(path);
                                    var shape = ilineshape.ConvertToShape();
                                    if (wrapp != WdWrapType.wdWrapInline)
                                    {
                                        shape.WrapFormat.Type = WdWrapType.wdWrapInline;//  wrapp;
                                    }
                                    else
                                    {
                                        shape.WrapFormat.Type = WdWrapType.wdWrapInline;
                                    }

                                    if (Height != 0 && Width != 0)
                                    {
                                        shape.WrapFormat.Type = WdWrapType.wdWrapInline; //wrapp;
                                        shape.Width = Width;
                                        shape.Height = Height;
                                    }

                                    shape.LockAspectRatio = MsoTriState.msoCTrue;
                                    //item.Title = dataTable.Name;
                                    //item.Tag = excelVM.FullPath;
                                    System.IO.File.Delete(path);
                                }
                                catch (Exception ex)
                                {

                                    MessageBox.Show(string.Format("Error : {0}", ex.Message), "DataBridge.Word");
                                }

                            }
                        }
                        else
                        {
                            MessageBox.Show(string.Format("Error : {0}", result.ErrorMessage), "DataBridge.Word");
                        }
                    }
                    if (item.Title.Contains(Constants.IPPrefix))
                    {
                        ServiceResult result = OpenAndRefreshExcel(excelVM, item.Title, DataType.InfoPoint);
                        if (result.Sucess)
                        {

                            IEnumerable<DataTable> dataTables = wordService.GetDataTables(wordVM, excelVM);
                            DataTable dataTable = dataTables.FirstOrDefault(x => x.Name == item.Title && x.Type == DataType.InfoPoint);
                            if (dataTable.Value == null || dataTable.Value == string.Empty)
                            {
                                item.Range.Text = dataTable.Name;
                                item.Range.HighlightColorIndex = WdColorIndex.wdYellow;
                            }
                            else
                            {
                                item.Range.HighlightColorIndex = WdColorIndex.wdNoHighlight;
                                item.Range.Text = dataTable.Value;
                            }
                        }
                        else
                        {
                            MessageBox.Show(string.Format("Error : {0}", result.ErrorMessage), "DataBridge.Word");
                        }
                    }
                }
            }
            Globals.ThisAddIn.Application.ScreenUpdating = true;
        }

        private void FinalBtn_Click(object sender, RibbonControlEventArgs e)
        {
            for (int i = 0; i < Globals.ThisAddIn.Application.ActiveDocument.ContentControls.Count; i++)
            {
                var item = Globals.ThisAddIn.Application.ActiveDocument.ContentControls[i + 1];
                item.Tag = "Final";
                //item.Title = "Final";
            }

            WordVM wordVM = new WordVM();
            wordVM.Path = Globals.ThisAddIn.Application.ActiveDocument.Path;
            wordVM.Name = Globals.ThisAddIn.Application.ActiveDocument.Name;

            WordService wordService = new WordService();
            ServiceResult result = wordService.ClearWord(wordVM);

            if (result.Sucess)
            {
                Globals.Ribbons.WordRibbon.ListOfAssociatedExcel.Items.Clear();
                Globals.Ribbons.WordRibbon.ListOfAvailableInfoPoints.Items.Clear();
                Globals.Ribbons.WordRibbon.ListOfAvailableTablePictures.Items.Clear();
            }
            else
            {
                MessageBox.Show(string.Format("Error : {0}", result.ErrorMessage), "DataBridge.Word");
            }

        }

        private void HighlightBtn_Click(object sender, RibbonControlEventArgs e)
        {
            var word = Globals.ThisAddIn.Application.ActiveDocument;

            WordVM wordVM = new WordVM();
            wordVM.Path = Globals.ThisAddIn.Application.ActiveDocument.Path;
            wordVM.Name = Globals.ThisAddIn.Application.ActiveDocument.Name;

            if (!_highlightDataPoints.ContainsKey(wordVM.Name))
            {
                _highlightDataPoints[wordVM.Name] = false;
            }

            if (_highlightDataPoints[wordVM.Name])
            {
                for (int i = 0; i < Globals.ThisAddIn.Application.ActiveDocument.ContentControls.Count; i++)
                {
                    try
                    {
                        var item = Globals.ThisAddIn.Application.ActiveDocument.ContentControls[i + 1];
                        if (item.Type == WdContentControlType.wdContentControlRichText)
                        {
                            item.Range.HighlightColorIndex = WdColorIndex.wdWhite;
                        }
                    }
                    catch (Exception)
                    {

                    }
                    //word.ToggleFormsDesign();
                }
                _highlightDataPoints[wordVM.Name] = false;
            }
            else
            {
                for (int i = 0; i < Globals.ThisAddIn.Application.ActiveDocument.ContentControls.Count; i++)
                {
                    try
                    {
                        var item = Globals.ThisAddIn.Application.ActiveDocument.ContentControls[i + 1];
                        if (item.Type == WdContentControlType.wdContentControlRichText)
                        {
                            item.Range.HighlightColorIndex = WdColorIndex.wdYellow;
                        }
                    }
                    catch (Exception)
                    {

                    }
                    //word.ToggleFormsDesign();
                }
                _highlightDataPoints[wordVM.Name] = true;
            }

        }

        private void CreateGraphic_Click(object sender, RibbonControlEventArgs e)
        {
            CreateGraphicsForm createGraphicsForm = new CreateGraphicsForm();
            createGraphicsForm.Show();
        }

        private void ManageGraphics_Click(object sender, RibbonControlEventArgs e)
        {
            MessageBox.Show("Not implemented yet", "DataBridge.Word");
        }

        private void UpdateGraphics_Click(object sender, RibbonControlEventArgs e)
        {
            MessageBox.Show("Not implemented yet", "DataBridge.Word");
        }
    }
}

