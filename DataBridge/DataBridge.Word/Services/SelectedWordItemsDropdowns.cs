﻿using Microsoft.Office.Tools.Ribbon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBridge.Word.Services
{
    public class SelectedWordItemsDropdowns
    {
        public RibbonDropDownItem Excel { get; set; }
        public RibbonDropDownItem Ip { get; set; }
        public RibbonDropDownItem Tp { get; set; }
      
    }
}
